package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.LogicDepositListener;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import com.luobo.pcsdepositmanage.service.basic.LogicDepositService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetLogicDepositPara extends MethodTemplate {
    public static GetLogicDepositPara getLogicDepositPara;
    @Autowired
    protected LogicDepositService logicDepositService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.logicdeposit}")
    private String defaultFile;
    @Value("${sheetname.logicdeposit}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getLogicDepositPara = this;
        getLogicDepositPara.defaultDir = this.defaultDir;
        getLogicDepositPara.defaultFile = this.defaultFile;
        getLogicDepositPara.defaultSheet = this.defaultSheet;
        getLogicDepositPara.logicDepositService = this.logicDepositService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", LogicDeposit.class);
        paraMap.put("listener",new LogicDepositListener(logicDepositService));
        paraMap.put("servicename",logicDepositService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
