package com.luobo.pcsdepositmanage.method;


import java.util.List;
import java.util.Map;

public abstract class AbstractMethodTemp {


    public List<Map<String,Object>> getListMapData(Map map) throws Exception {
        throw new UnsupportedOperationException();
    }

    public int WriteDataToMysql(List<Object> tList,Map map) throws Exception{
        throw new UnsupportedOperationException();
    }

    public void WriteDataToExcelFile(List<String> titlelist, List<Map<String,Object>> data,Map map) throws Exception {
        throw new UnsupportedOperationException();
    }


}
