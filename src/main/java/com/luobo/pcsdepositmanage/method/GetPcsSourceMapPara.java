package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.sourcefile.PcsSourceMapListener;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import com.luobo.pcsdepositmanage.service.basic.PcsDepositService;
import com.luobo.pcsdepositmanage.service.sourcefile.PcsSourceMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
@Component
public class GetPcsSourceMapPara extends MethodTemplate {
    public static GetPcsSourceMapPara getPcsSourceMapPara;
    @Autowired
    protected PcsSourceMapService pcsSourceMapService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.pcssourcemap}")
    private String defaultFile;
    @Value("${sheetname.pcssourcemap}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getPcsSourceMapPara = this;
        getPcsSourceMapPara.defaultDir = this.defaultDir;
        getPcsSourceMapPara.defaultFile = this.defaultFile;
        getPcsSourceMapPara.defaultSheet = this.defaultSheet;
        getPcsSourceMapPara.pcsSourceMapService = this.pcsSourceMapService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", PcsSourceMap.class);
        paraMap.put("listener",new PcsSourceMapListener(pcsSourceMapService));
        paraMap.put("servicename",pcsSourceMapService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
