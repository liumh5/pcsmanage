package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositDestsystemListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositListener;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositDestsystem;
import com.luobo.pcsdepositmanage.service.basic.PcsDepositDestsystemService;
import com.luobo.pcsdepositmanage.service.basic.PcsDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetPcsDepositDestsystemPara extends MethodTemplate {
    public static GetPcsDepositDestsystemPara getPcsDepositDestsystemPara;
    @Autowired
    protected PcsDepositDestsystemService pcsDepositDestsystemService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.pcsdepositdestsystem}")
    private String defaultFile;
    @Value("${sheetname.pcsdepositdestsystem}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getPcsDepositDestsystemPara = this;
        getPcsDepositDestsystemPara.defaultDir = this.defaultDir;
        getPcsDepositDestsystemPara.defaultFile = this.defaultFile;
        getPcsDepositDestsystemPara.defaultSheet = this.defaultSheet;
        getPcsDepositDestsystemPara.pcsDepositDestsystemService = this.pcsDepositDestsystemService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", PcsDepositDestsystem.class);
        paraMap.put("listener",new PcsDepositDestsystemListener(pcsDepositDestsystemService));
        paraMap.put("servicename",pcsDepositDestsystemService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
