package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.resultfile.PcsDepositResultListener;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.pojo.resultfile.PcsDepositResult;
import com.luobo.pcsdepositmanage.service.resultfile.PcsDepositResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetPcsDepositResultPara extends MethodTemplate{
    public static GetPcsDepositResultPara getPcsDepositResultPara;
    @Autowired
    protected PcsDepositResultService pcsDepositResultService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.pcsdepositresult}")
    private String defaultFile;
    @Value("${sheetname.pcsdepositresult}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getPcsDepositResultPara = this;
        getPcsDepositResultPara.defaultDir = this.defaultDir;
        getPcsDepositResultPara.defaultFile = this.defaultFile;
        getPcsDepositResultPara.defaultSheet = this.defaultSheet;
        getPcsDepositResultPara.pcsDepositResultService = this.pcsDepositResultService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", PcsDepositResult.class);
        paraMap.put("listener",new PcsDepositResultListener(pcsDepositResultService));
        paraMap.put("servicename",pcsDepositResultService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
