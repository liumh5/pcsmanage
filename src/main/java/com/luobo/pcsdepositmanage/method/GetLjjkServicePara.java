package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.sourcefile.LjjkServiceListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.sourcefile.PcsSourceMapListener;
import com.luobo.pcsdepositmanage.pojo.sourcefile.LjjkService;
import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import com.luobo.pcsdepositmanage.service.sourcefile.LjjkServiceService;
import com.luobo.pcsdepositmanage.service.sourcefile.PcsSourceMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetLjjkServicePara extends MethodTemplate {
    public static GetLjjkServicePara getLjjkServicePara;
    @Autowired
    protected LjjkServiceService ljjkServiceService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.ljjkservice}")
    private String defaultFile;
    @Value("${sheetname.ljjkservice}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getLjjkServicePara = this;
        getLjjkServicePara.defaultDir = this.defaultDir;
        getLjjkServicePara.defaultFile = this.defaultFile;
        getLjjkServicePara.defaultSheet = this.defaultSheet;
        getLjjkServicePara.ljjkServiceService = this.ljjkServiceService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", LjjkService.class);
        paraMap.put("listener",new LjjkServiceListener(ljjkServiceService));
        paraMap.put("servicename",ljjkServiceService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
