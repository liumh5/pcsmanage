package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.LogicDepositPcsListener;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import com.luobo.pcsdepositmanage.service.basic.LogicDepositPcsService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetLogicDepositPcsPara extends MethodTemplate {
    public static GetLogicDepositPcsPara getLogicDepositPcsPara;
    @Autowired
    protected LogicDepositPcsService logicDepositPcsService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.logicdepositpcs}")
    private String defaultFile;
    @Value("${sheetname.logicdepositpcs}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getLogicDepositPcsPara = this;
        getLogicDepositPcsPara.defaultDir = this.defaultDir;
        getLogicDepositPcsPara.defaultFile = this.defaultFile;
        getLogicDepositPcsPara.defaultSheet = this.defaultSheet;
        getLogicDepositPcsPara.logicDepositPcsService = this.logicDepositPcsService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", LogicDepositPcs.class);
        paraMap.put("listener",new LogicDepositPcsListener(logicDepositPcsService));
        paraMap.put("servicename",logicDepositPcsService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
