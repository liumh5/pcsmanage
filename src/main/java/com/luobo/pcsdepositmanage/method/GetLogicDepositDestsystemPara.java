package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.LogicDepositDestsystemListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.basic.LogicDepositPcsListener;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import com.luobo.pcsdepositmanage.service.basic.LogicDepositDestsystemService;
import com.luobo.pcsdepositmanage.service.basic.LogicDepositPcsService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetLogicDepositDestsystemPara extends MethodTemplate {
    public static GetLogicDepositDestsystemPara getLogicDepositDestsystemPara;
    @Autowired
    protected LogicDepositDestsystemService logicDepositDestsystemService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.logicdepositdestsystem}")
    private String defaultFile;
    @Value("${sheetname.logicdepositdestsystem}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getLogicDepositDestsystemPara = this;
        getLogicDepositDestsystemPara.defaultDir = this.defaultDir;
        getLogicDepositDestsystemPara.defaultFile = this.defaultFile;
        getLogicDepositDestsystemPara.defaultSheet = this.defaultSheet;
        getLogicDepositDestsystemPara.logicDepositDestsystemService = this.logicDepositDestsystemService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", LogicDepositDestsystem.class);
        paraMap.put("listener",new LogicDepositDestsystemListener(logicDepositDestsystemService));
        paraMap.put("servicename",logicDepositDestsystemService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
