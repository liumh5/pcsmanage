package com.luobo.pcsdepositmanage.method;

import com.alibaba.druid.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class MethodTempFactory {
    private static Map<String,MethodTemplate> strategyMap = new HashMap<String,MethodTemplate>();
    public static MethodTemplate getInvokeStrategy(String name){
        return strategyMap.get(name);
    }
    public static void register (String name ,MethodTemplate strategy){
        if(StringUtils.isEmpty(name) || strategy == null){
            return ;
        }
        strategyMap.put(name,strategy);
    }
}
