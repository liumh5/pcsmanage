package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.basic.PcsDepositLogicListener;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositLogic;
import com.luobo.pcsdepositmanage.service.basic.PcsDepositLogicService;
import com.luobo.pcsdepositmanage.service.basic.PcsDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetPcsDepositLogicPara extends MethodTemplate{
    public static GetPcsDepositLogicPara getPcsDepositLogicPara;
    @Autowired
    protected PcsDepositLogicService pcsDepositLogicService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filename.pcsdepositlogic}")
    private String defaultFile;
    @Value("${sheetname.pcsdepositlogic}")
    private String defaultSheet;


    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getPcsDepositLogicPara = this;
        getPcsDepositLogicPara.defaultDir = this.defaultDir;
        getPcsDepositLogicPara.defaultFile = this.defaultFile;
        getPcsDepositLogicPara.defaultSheet = this.defaultSheet;
        getPcsDepositLogicPara.pcsDepositLogicService = this.pcsDepositLogicService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = defaultFile;
        String sheetname = defaultSheet;
        String filedir = defaultDir;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("clazz", PcsDepositLogic.class);
        paraMap.put("listener",new PcsDepositLogicListener(pcsDepositLogicService));
        paraMap.put("servicename",pcsDepositLogicService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
