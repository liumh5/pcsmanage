package com.luobo.pcsdepositmanage.method;

import com.luobo.pcsdepositmanage.easyexcel.listener.databasetest.SysDictDataListener;
import com.luobo.pcsdepositmanage.easyexcel.listener.sourcefile.PcsSourceMapListener;
import com.luobo.pcsdepositmanage.pojo.databasetest.SysDictData;
import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import com.luobo.pcsdepositmanage.service.databasetest.SysDictDataService;
import com.luobo.pcsdepositmanage.service.sourcefile.PcsSourceMapService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
@Data
public class GetSysDictDataPara extends MethodTemplate {
    public static GetSysDictDataPara getSysDictDataPara;
    @Autowired
    protected SysDictDataService sysDictDataService;

    @Value("${filedir.name1}")
    private String defaultDir;
    @Value("${filedir.sysdictfile}")
    private String defaultFile;
    @Value("${filedir.sysdictsheet}")
    private String defaultSheet;
    @Value("${filedir.sysdictaheadnum}")
    private int defaultheadnum;

    @PostConstruct//通过@PostConstruct 实现初始化bean之前进行操作
    public void init(){

        getSysDictDataPara = this;
        getSysDictDataPara.defaultDir = this.defaultDir;
        getSysDictDataPara.defaultFile = this.defaultFile;
        getSysDictDataPara.defaultSheet = this.defaultSheet;
        getSysDictDataPara.defaultheadnum = this.defaultheadnum;
        getSysDictDataPara.sysDictDataService = this.sysDictDataService;

    }

    @Override
    public Map<String, Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        Map<String ,Object> paraMap = new HashMap<String ,Object>();
        String filename = this.defaultFile;
        String sheetname = this.defaultSheet;
        String filedir = this.defaultDir;
        int headrownum = this.defaultheadnum+1;
        if(map.containsKey("filename")){
            filename = (String) map.get("filename");
        }
        if(map.containsKey("sheetname")){
            sheetname = (String) map.get("sheetname");
        }
        if(map.containsKey("filedir")){
            filedir = filedir + "/" + map.get("filedir");
        }
        if(map.containsKey("headrownum")){
            headrownum = (Integer) map.get("headrownum");
        }

        paraMap.put("filedir",filedir+"/");
        paraMap.put("filename",filename);
        paraMap.put("sheetname",sheetname);
        paraMap.put("headrownum",headrownum);

        paraMap.put("clazz", SysDictData.class);
        paraMap.put("listener",new SysDictDataListener(this.sysDictDataService));
        paraMap.put("servicename",this.sysDictDataService);
//        System.out.println(paraMap);
        return paraMap;
    }
}
