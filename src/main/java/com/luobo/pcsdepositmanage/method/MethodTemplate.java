package com.luobo.pcsdepositmanage.method;

import java.util.List;
import java.util.Map;

public abstract class MethodTemplate {

    public Map<String,Object> getParaMapTemplate(Map<String,Object> map) throws Exception {
        throw new UnsupportedOperationException();
    }
}
