package com.luobo.pcsdepositmanage.dao.sourcefile;

import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface PcsSourceMapMapper {
    int queryCount();
    List<PcsSourceMap> queryAllData();
    List<PcsSourceMap> queryDataForMap(Map maps);
    int delData(PcsSourceMap dataObject);
    int addData(PcsSourceMap dataObject);
    int batchAddData(@Param("list") List<PcsSourceMap> list);
}
