package com.luobo.pcsdepositmanage.dao.sourcefile;

import com.luobo.pcsdepositmanage.pojo.sourcefile.LjjkService;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface LjjkServiceMapper {
    int queryCount();
    List<LjjkService> queryAllData();
    List<LjjkService> queryDataForMap(Map maps);
    int delData(LjjkService dataObject);
    int addData(LjjkService dataObject);
    int batchAddData(@Param("list") List<LjjkService> list);
}
