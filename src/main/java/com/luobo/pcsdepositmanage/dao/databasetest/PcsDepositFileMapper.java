package com.luobo.pcsdepositmanage.dao.databasetest;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PcsDepositFileMapper {
    int queryCount(Map map);
    List<PcsDepositFile> queryAllData();
    List<PcsDepositFile> queryDataForMap(Map maps);
    int delData(PcsDepositFile dataObject);
    int addData(PcsDepositFile dataObject);
    int batchAddData(@Param("list") List<PcsDepositFile> list);
}
