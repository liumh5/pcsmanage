package com.luobo.pcsdepositmanage.dao.databasetest;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PcsInOutDescribeMapper {
    int queryCount(Map map);
    List<PcsInOutDescribe> queryAllData();
    List<PcsInOutDescribe> queryDataForMap(Map maps);
    int delData(PcsInOutDescribe dataObject);
    int addData(PcsInOutDescribe dataObject);
    int batchAddData(@Param("list") List<PcsInOutDescribe> list);
}
