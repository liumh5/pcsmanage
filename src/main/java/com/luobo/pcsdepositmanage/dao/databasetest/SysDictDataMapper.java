package com.luobo.pcsdepositmanage.dao.databasetest;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.pojo.databasetest.SysDictData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SysDictDataMapper {
    int queryCount(Map map);
    List<SysDictData> queryAllData();
    List<SysDictData> queryDataForMap(Map maps);
    int delData(SysDictData dataObject);
    int addData(SysDictData dataObject);
    int batchAddData(@Param("list") List<SysDictData> list);
}

