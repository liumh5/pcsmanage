package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface LogicDepositMapper {
    int queryCount();
    List<LogicDeposit> queryAllData();
    List<LogicDeposit> queryDataForMap(Map maps);
    int delData(LogicDeposit dataObject);
    int addData(LogicDeposit dataObject);
    int batchAddData(@Param("list") List<LogicDeposit> list);
}
