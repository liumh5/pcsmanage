package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface LogicDepositDestsystemMapper {
    int queryCount();
    List<LogicDepositDestsystem> queryAllData();
    List<LogicDepositDestsystem> queryDataForMap(Map maps);
    int delData(LogicDepositDestsystem dataObject);
    int addData(LogicDepositDestsystem dataObject);
    int batchAddData(@Param("list") List<LogicDepositDestsystem> list);
}
