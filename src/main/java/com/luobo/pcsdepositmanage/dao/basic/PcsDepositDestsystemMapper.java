package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositDestsystem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface PcsDepositDestsystemMapper {
    int queryCount();
    List<PcsDepositDestsystem> queryAllData();
    List<PcsDepositDestsystem> queryDataForMap(Map maps);
    int delData(PcsDepositDestsystem dataObject);
    int addData(PcsDepositDestsystem dataObject);
    int batchAddData(@Param("list") List<PcsDepositDestsystem> list);
}
