package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositLogic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PcsDepositLogicMapper {
    int queryCount();
    List<PcsDepositLogic> queryAllData();
    List<PcsDepositLogic> queryDataForMap(Map maps);
    int delData(PcsDepositLogic dataObject);
    int addData(PcsDepositLogic dataObject);
    int batchAddData(@Param("list") List<PcsDepositLogic> list);
}
