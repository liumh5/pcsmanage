package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface LogicDepositPcsMapper {
    int queryCount();
    List<LogicDepositPcs> queryAllData();
    List<LogicDepositPcs> queryDataForMap(Map maps);
    int delData(LogicDepositPcs dataObject);
    int addData(LogicDepositPcs dataObject);
    int batchAddData(@Param("list") List<LogicDepositPcs> list);
}
