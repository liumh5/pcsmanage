package com.luobo.pcsdepositmanage.dao.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface PcsDepositMapper {
    int queryCount();
    List<PcsDeposit> queryAllData();
    List<PcsDeposit> queryDataForMap(Map maps);
    int delData(PcsDeposit dataObject);
    int addData(PcsDeposit dataObject);
    int batchAddData(@Param("list") List<PcsDeposit> list);
}
