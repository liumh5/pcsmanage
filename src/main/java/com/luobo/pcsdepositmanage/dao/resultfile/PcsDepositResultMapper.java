package com.luobo.pcsdepositmanage.dao.resultfile;

import com.luobo.pcsdepositmanage.pojo.resultfile.PcsDepositResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PcsDepositResultMapper {
    int queryCount();
    List<PcsDepositResult> queryAllData();
    List<PcsDepositResult> queryDataForMap(Map maps);
    int delData(PcsDepositResult dataObject);
    int addData(PcsDepositResult dataObject);
    int batchAddData(@Param("list") List<PcsDepositResult> list);
}
