package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.dao.basic.LogicDepositDestsystemMapper;
import com.luobo.pcsdepositmanage.dao.databasetest.PcsInOutDescribeMapper;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class PcsInOutDescribeServiceImpl implements PcsInOutDescribeService {


    @Autowired
    private PcsInOutDescribeMapper pcsInOutDescribeMapper;
    @Override
    public int queryCount(Map map) {
        return pcsInOutDescribeMapper.queryCount(map);
    }

    @Override
    public List<PcsInOutDescribe> queryAllData() {
        return pcsInOutDescribeMapper.queryAllData();
    }

    @Override
    public List<PcsInOutDescribe> queryDataForMap(Map map) {
        return pcsInOutDescribeMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsInOutDescribe pcsInOutDescribe) {
        return pcsInOutDescribeMapper.delData(pcsInOutDescribe);
    }

    @Override
    public int addData(PcsInOutDescribe pcsInOutDescribe) {
        return pcsInOutDescribeMapper.addData(pcsInOutDescribe);
    }

    @Override
    public int batchAddData(List<PcsInOutDescribe> pcsInOutDescribeList) {
        return pcsInOutDescribeMapper.batchAddData(pcsInOutDescribeList);
    }
}
