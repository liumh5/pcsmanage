package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsDepositFileService extends DbService<PcsDepositFile> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsDepositFile> queryAllData();

    @Override
    List<PcsDepositFile> queryDataForMap(Map map);

    @Override
    int delData(PcsDepositFile pcsDepositFile);

    @Override
    int addData(PcsDepositFile pcsDepositFile);

    @Override
    int batchAddData(List<PcsDepositFile> pcsDepositFile);
}
