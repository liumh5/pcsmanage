package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.dao.databasetest.SysDictDataMapper;
import com.luobo.pcsdepositmanage.pojo.databasetest.SysDictData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SysDictDataServiceImpl implements SysDictDataService{

    @Autowired
    private SysDictDataMapper sysDictDataMapper;


    @Override
    public int queryCount(Map map) {
        return this.sysDictDataMapper.queryCount(map);
    }

    @Override
    public List<SysDictData> queryAllData() {
        return this.sysDictDataMapper.queryAllData();
    }

    @Override
    public List<SysDictData> queryDataForMap(Map map) {
        return this.sysDictDataMapper.queryDataForMap(map);
    }

    @Override
    public int delData(SysDictData sysDictData) {
        return this.sysDictDataMapper.delData(sysDictData);
    }

    @Override
    public int addData(SysDictData sysDictData) {
        return this.sysDictDataMapper.addData(sysDictData);
    }

    @Override
    public int batchAddData(List<SysDictData> sysDictData) {
        return this.sysDictDataMapper.batchAddData(sysDictData);
    }
}
