package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsInOutDescribeService  extends DbService<PcsInOutDescribe> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsInOutDescribe> queryAllData();

    @Override
    List<PcsInOutDescribe> queryDataForMap(Map map);

    @Override
    int delData(PcsInOutDescribe pcsInOutDescribe);

    @Override
    int addData(PcsInOutDescribe pcsInOutDescribe);

    @Override
    int batchAddData(List<PcsInOutDescribe> pcsInOutDescribe);
}
