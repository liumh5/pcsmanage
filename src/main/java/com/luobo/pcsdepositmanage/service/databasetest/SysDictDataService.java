package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import com.luobo.pcsdepositmanage.pojo.databasetest.SysDictData;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface SysDictDataService extends DbService<SysDictData> {
    @Override
    int queryCount(Map map);

    @Override
    List<SysDictData> queryAllData();

    @Override
    List<SysDictData> queryDataForMap(Map map);

    @Override
    int delData(SysDictData sysDictData);

    @Override
    int addData(SysDictData sysDictData);

    @Override
    int batchAddData(List<SysDictData> sysDictData);
}
