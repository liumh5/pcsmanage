package com.luobo.pcsdepositmanage.service.databasetest;

import com.luobo.pcsdepositmanage.dao.databasetest.PcsDepositFileMapper;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsDepositFileServiceImpl implements PcsDepositFileService {
    @Autowired
    private PcsDepositFileMapper pcsDepositFileMapper;

    @Override
    public int queryCount(Map map) {
        return pcsDepositFileMapper.queryCount(map);
    }

    @Override
    public List<PcsDepositFile> queryAllData() {
        return pcsDepositFileMapper.queryAllData();
    }

    @Override
    public List<PcsDepositFile> queryDataForMap(Map map) {
        return pcsDepositFileMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsDepositFile pcsDepositFile) {
        return pcsDepositFileMapper.delData(pcsDepositFile);
    }

    @Override
    public int addData(PcsDepositFile pcsDepositFile) {
        return pcsDepositFileMapper.addData(pcsDepositFile);
    }

    @Override
    public int batchAddData(List<PcsDepositFile> pcsDepositFiles) {
        return pcsDepositFileMapper.batchAddData(pcsDepositFiles);
    }
}
