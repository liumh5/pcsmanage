package com.luobo.pcsdepositmanage.service.sourcefile;

import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsSourceMapService extends DbService<PcsSourceMap> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsSourceMap> queryAllData();

    @Override
    List<PcsSourceMap> queryDataForMap(Map map);

    @Override
    int delData(PcsSourceMap pcsSourceMap);

    @Override
    int addData(PcsSourceMap pcsSourceMap);

    @Override
    int batchAddData(List<PcsSourceMap> pcsSourceMaps);
}
