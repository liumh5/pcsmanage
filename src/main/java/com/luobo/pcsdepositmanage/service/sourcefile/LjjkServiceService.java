package com.luobo.pcsdepositmanage.service.sourcefile;

import com.luobo.pcsdepositmanage.pojo.sourcefile.LjjkService;
import com.luobo.pcsdepositmanage.service.DbService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LjjkServiceService extends DbService<LjjkService> {
    @Override
    int queryCount(Map map);
    @Override
    List<LjjkService> queryAllData();
    @Override
    List<LjjkService> queryDataForMap(Map maps);
    @Override
    int delData(LjjkService ljjkService);
    @Override
    int addData(LjjkService ljjkService);
    @Override
    int batchAddData(@Param("list") List<LjjkService> list);
}
