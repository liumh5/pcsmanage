package com.luobo.pcsdepositmanage.service.sourcefile;

import com.luobo.pcsdepositmanage.dao.sourcefile.LjjkServiceMapper;
import com.luobo.pcsdepositmanage.pojo.sourcefile.LjjkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LjjkServiceServiceImpl implements LjjkServiceService {
    @Autowired
    private LjjkServiceMapper ljjkServiceMapper;

    @Override
    public int queryCount(Map map) {
        return ljjkServiceMapper.queryCount();
    }

    @Override
    public List<LjjkService> queryAllData() {
        return ljjkServiceMapper.queryAllData();
    }

    @Override
    public List<LjjkService> queryDataForMap(Map maps) {
        return ljjkServiceMapper.queryDataForMap(maps);
    }

    @Override
    public int delData(LjjkService ljjkService) {
        return ljjkServiceMapper.delData(ljjkService);
    }

    @Override
    public int addData(LjjkService ljjkService) {
        return ljjkServiceMapper.addData(ljjkService);
    }

    @Override
    public int batchAddData(List<LjjkService> list) {
        return ljjkServiceMapper.batchAddData(list);
    }
}
