package com.luobo.pcsdepositmanage.service.sourcefile;

import com.luobo.pcsdepositmanage.dao.sourcefile.PcsSourceMapMapper;
import com.luobo.pcsdepositmanage.pojo.sourcefile.PcsSourceMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsSourceMapServiceImpl implements PcsSourceMapService{
    @Autowired
    private PcsSourceMapMapper pcsSourceMapMapper;
    @Override
    public int queryCount(Map map) {
        return pcsSourceMapMapper.queryCount();
    }

    @Override
    public List<PcsSourceMap> queryAllData() {
        return pcsSourceMapMapper.queryAllData();
    }

    @Override
    public List<PcsSourceMap> queryDataForMap(Map map) {
        return pcsSourceMapMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsSourceMap pcsSourceMap) {
        return pcsSourceMapMapper.delData(pcsSourceMap);
    }

    @Override
    public int addData(PcsSourceMap pcsSourceMap) {
        return pcsSourceMapMapper.addData(pcsSourceMap);
    }

    @Override
    public int batchAddData(List<PcsSourceMap> pcsSourceMaps) {
        return pcsSourceMapMapper.batchAddData(pcsSourceMaps);
    }
}
