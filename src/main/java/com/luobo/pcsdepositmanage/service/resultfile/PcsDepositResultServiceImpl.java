package com.luobo.pcsdepositmanage.service.resultfile;

import com.luobo.pcsdepositmanage.dao.resultfile.PcsDepositResultMapper;
import com.luobo.pcsdepositmanage.pojo.resultfile.PcsDepositResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsDepositResultServiceImpl implements PcsDepositResultService{

    @Autowired
    private PcsDepositResultMapper pcsDepositResultMapper;

    @Override
    public int queryCount(Map map) {
        return pcsDepositResultMapper.queryCount();
    }

    @Override
    public List<PcsDepositResult> queryAllData() {
        return pcsDepositResultMapper.queryAllData();
    }

    @Override
    public List<PcsDepositResult> queryDataForMap(Map map) {
        return pcsDepositResultMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsDepositResult pcsDepositResult) {
        return pcsDepositResultMapper.delData(pcsDepositResult);
    }

    @Override
    public int addData(PcsDepositResult pcsDepositResult) {
        return pcsDepositResultMapper.addData(pcsDepositResult);
    }

    @Override
    public int batchAddData(List<PcsDepositResult> pcsDepositResults) {
        return pcsDepositResultMapper.batchAddData(pcsDepositResults);
    }
}
