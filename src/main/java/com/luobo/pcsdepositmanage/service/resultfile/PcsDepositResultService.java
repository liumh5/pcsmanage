package com.luobo.pcsdepositmanage.service.resultfile;

import com.luobo.pcsdepositmanage.pojo.resultfile.PcsDepositResult;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsDepositResultService extends DbService<PcsDepositResult> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsDepositResult> queryAllData();

    @Override
    List<PcsDepositResult> queryDataForMap(Map map);

    @Override
    int delData(PcsDepositResult pcsDepositResult);

    @Override
    int addData(PcsDepositResult pcsDepositResult);

    @Override
    int batchAddData(List<PcsDepositResult> pcsDepositResults);
}
