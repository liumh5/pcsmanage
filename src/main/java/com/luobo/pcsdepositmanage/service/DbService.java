package com.luobo.pcsdepositmanage.service;

import java.util.List;
import java.util.Map;

public interface DbService<T> {
    int queryCount(Map map);
    List<T> queryAllData();
    List<T> queryDataForMap(Map map);
    int delData(T t);
    int addData(T t);
    int batchAddData(List<T> tList);
}
