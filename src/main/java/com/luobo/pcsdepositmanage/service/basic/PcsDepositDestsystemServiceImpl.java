package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.PcsDepositDestsystemMapper;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositDestsystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsDepositDestsystemServiceImpl implements PcsDepositDestsystemService {

    @Autowired
    private PcsDepositDestsystemMapper pcsDepositDestsystemMapper;

    @Override
    public int queryCount(Map map) {
        return pcsDepositDestsystemMapper.queryCount();
    }

    @Override
    public List<PcsDepositDestsystem> queryAllData() {
        return pcsDepositDestsystemMapper.queryAllData();
    }

    @Override
    public List<PcsDepositDestsystem> queryDataForMap(Map map) {
        return pcsDepositDestsystemMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsDepositDestsystem pcsDepositDestsystem) {
        return pcsDepositDestsystemMapper.delData(pcsDepositDestsystem);
    }

    @Override
    public int addData(PcsDepositDestsystem pcsDepositDestsystem) {
        return pcsDepositDestsystemMapper.addData(pcsDepositDestsystem);
    }

    @Override
    public int batchAddData(List<PcsDepositDestsystem> pcsDepositDestsystems) {
        return pcsDepositDestsystemMapper.batchAddData(pcsDepositDestsystems);
    }
}
