package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface LogicDepositDestsystemService extends DbService<LogicDepositDestsystem> {
    @Override
    int queryCount(Map map);

    @Override
    List<LogicDepositDestsystem> queryAllData();

    @Override
    List<LogicDepositDestsystem> queryDataForMap(Map map);

    @Override
    int delData(LogicDepositDestsystem logicDepositDestsystem);

    @Override
    int addData(LogicDepositDestsystem logicDepositDestsystem);

    @Override
    int batchAddData(List<LogicDepositDestsystem> logicDepositDestsystems);
}
