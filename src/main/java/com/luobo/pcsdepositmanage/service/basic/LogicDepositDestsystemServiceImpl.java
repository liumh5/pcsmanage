package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.LogicDepositDestsystemMapper;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class LogicDepositDestsystemServiceImpl implements LogicDepositDestsystemService {
    @Autowired
    private LogicDepositDestsystemMapper logicDepositDestsystemMapper;

    @Override
    public int queryCount(Map map) {
        return logicDepositDestsystemMapper.queryCount();
    }

    @Override
    public List<LogicDepositDestsystem> queryAllData() {
        return logicDepositDestsystemMapper.queryAllData();
    }

    @Override
    public List<LogicDepositDestsystem> queryDataForMap(Map map) {
        return logicDepositDestsystemMapper.queryDataForMap(map);
    }

    @Override
    public int delData(LogicDepositDestsystem logicDepositDestsystem) {
        return logicDepositDestsystemMapper.delData(logicDepositDestsystem);
    }

    @Override
    public int addData(LogicDepositDestsystem logicDepositDestsystem) {
        return logicDepositDestsystemMapper.addData(logicDepositDestsystem);
    }

    @Override
    public int batchAddData(List<LogicDepositDestsystem> logicDepositDestsystems) {
        return logicDepositDestsystemMapper.batchAddData(logicDepositDestsystems);
    }
}
