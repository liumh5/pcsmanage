package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositLogic;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsDepositLogicService extends DbService<PcsDepositLogic> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsDepositLogic> queryAllData();

    @Override
    List<PcsDepositLogic> queryDataForMap(Map map);

    @Override
    int delData(PcsDepositLogic pcsDepositLogic);

    @Override
    int addData(PcsDepositLogic pcsDepositLogic);

    @Override
    int batchAddData(List<PcsDepositLogic> pcsDepositLogics);
}
