package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.PcsDepositMapper;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsDepositServiceImpl implements PcsDepositService{

    @Autowired
    private PcsDepositMapper pcsDepositMapper;
    @Override
    public int queryCount(Map map) {
        return pcsDepositMapper.queryCount();
    }

    @Override
    public List<PcsDeposit> queryAllData() {
        return pcsDepositMapper.queryAllData();
    }

    @Override
    public List<PcsDeposit> queryDataForMap(Map map) {
        return pcsDepositMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsDeposit pcsDeposit) {
        return pcsDepositMapper.delData(pcsDeposit);
    }

    @Override
    public int addData(PcsDeposit pcsDeposit) {
        return pcsDepositMapper.addData(pcsDeposit);
    }

    @Override
    public int batchAddData(List<PcsDeposit> pcsDeposits) {
        return pcsDepositMapper.batchAddData(pcsDeposits);
    }
}
