package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.PcsDepositLogicMapper;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PcsDepositLogicServiceImpl implements PcsDepositLogicService{
    @Autowired
    private PcsDepositLogicMapper pcsDepositLogicMapper;

    @Override
    public int queryCount(Map map) {
        return pcsDepositLogicMapper.queryCount();
    }

    @Override
    public List<PcsDepositLogic> queryAllData() {
        return pcsDepositLogicMapper.queryAllData();
    }

    @Override
    public List<PcsDepositLogic> queryDataForMap(Map map) {
        return pcsDepositLogicMapper.queryDataForMap(map);
    }

    @Override
    public int delData(PcsDepositLogic pcsDepositLogic) {
        return pcsDepositLogicMapper.delData(pcsDepositLogic);
    }

    @Override
    public int addData(PcsDepositLogic pcsDepositLogic) {
        return pcsDepositLogicMapper.addData(pcsDepositLogic);
    }

    @Override
    public int batchAddData(List<PcsDepositLogic> pcsDepositLogics) {
        return pcsDepositLogicMapper.batchAddData(pcsDepositLogics);
    }
}
