package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface LogicDepositPcsService extends DbService<LogicDepositPcs> {
    @Override
    int queryCount(Map map);

    @Override
    List<LogicDepositPcs> queryAllData();

    @Override
    List<LogicDepositPcs> queryDataForMap(Map map);

    @Override
    int delData(LogicDepositPcs logicDepositPcs);

    @Override
    int addData(LogicDepositPcs logicDepositPcs);

    @Override
    int batchAddData(List<LogicDepositPcs> logicDepositPcs);
}
