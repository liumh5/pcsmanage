package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface LogicDepositService extends DbService<LogicDeposit> {
    @Override
    int queryCount(Map map);

    @Override
    List<LogicDeposit> queryAllData();

    @Override
    List<LogicDeposit> queryDataForMap(Map map);

    @Override
    int delData(LogicDeposit logicDeposit);

    @Override
    int addData(LogicDeposit logicDeposit);

    @Override
    int batchAddData(List<LogicDeposit> logicDeposits);
}
