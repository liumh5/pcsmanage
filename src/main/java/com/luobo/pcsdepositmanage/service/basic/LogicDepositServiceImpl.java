package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.LogicDepositMapper;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogicDepositServiceImpl implements LogicDepositService {
    @Autowired
    private LogicDepositMapper logicDepositMapper;

    @Override
    public int queryCount(Map map) {
        return logicDepositMapper.queryCount();
    }

    @Override
    public List<LogicDeposit> queryAllData() {
        return logicDepositMapper.queryAllData();
    }

    @Override
    public List<LogicDeposit> queryDataForMap(Map map) {
        return logicDepositMapper.queryDataForMap(map);
    }

    @Override
    public int delData(LogicDeposit logicDeposit) {
        return logicDepositMapper.delData(logicDeposit);
    }

    @Override
    public int addData(LogicDeposit logicDeposit) {
        return logicDepositMapper.addData(logicDeposit);
    }

    @Override
    public int batchAddData(List<LogicDeposit> logicDeposits) {
        return logicDepositMapper.batchAddData(logicDeposits);
    }
}
