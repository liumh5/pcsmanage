package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.dao.basic.LogicDepositPcsMapper;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogicDepositPcsServiceImpl implements LogicDepositPcsService {
    @Autowired
    private LogicDepositPcsMapper logicDepositPcsMapper;
    @Override
    public int queryCount(Map map) {
        return logicDepositPcsMapper.queryCount();
    }

    @Override
    public List<LogicDepositPcs> queryAllData() {
        return logicDepositPcsMapper.queryAllData();
    }

    @Override
    public List<LogicDepositPcs> queryDataForMap(Map map) {
        return logicDepositPcsMapper.queryDataForMap(map);
    }

    @Override
    public int delData(LogicDepositPcs logicDepositPcs) {
        return logicDepositPcsMapper.delData(logicDepositPcs);
    }

    @Override
    public int addData(LogicDepositPcs logicDepositPcs) {
        return logicDepositPcsMapper.addData(logicDepositPcs);
    }

    @Override
    public int batchAddData(List<LogicDepositPcs> logicDepositPcs) {
        return logicDepositPcsMapper.batchAddData(logicDepositPcs);
    }
}
