package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDeposit;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsDepositService extends DbService<PcsDeposit> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsDeposit> queryAllData();

    @Override
    List<PcsDeposit> queryDataForMap(Map map);

    @Override
    int delData(PcsDeposit pcsDeposit);

    @Override
    int addData(PcsDeposit pcsDeposit);

    @Override
    int batchAddData(List<PcsDeposit> pcsDeposits);
}
