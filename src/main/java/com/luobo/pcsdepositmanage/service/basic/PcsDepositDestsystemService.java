package com.luobo.pcsdepositmanage.service.basic;

import com.luobo.pcsdepositmanage.pojo.basic.PcsDepositDestsystem;
import com.luobo.pcsdepositmanage.service.DbService;

import java.util.List;
import java.util.Map;

public interface PcsDepositDestsystemService extends DbService<PcsDepositDestsystem> {
    @Override
    int queryCount(Map map);

    @Override
    List<PcsDepositDestsystem> queryAllData();

    @Override
    List<PcsDepositDestsystem> queryDataForMap(Map map);

    @Override
    int delData(PcsDepositDestsystem pcsDepositDestsystem);

    @Override
    int addData(PcsDepositDestsystem pcsDepositDestsystem);

    @Override
    int batchAddData(List<PcsDepositDestsystem> pcsDepositDestsystems);
}
