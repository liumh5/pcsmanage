package com.luobo.pcsdepositmanage.util;

import java.util.*;

public class ListMapOperationUtil {


    public static List<Map<Integer,Object>> getAreaMapList(List<Map<Integer,Object>> mapList, int startRow, int endRow, int startCol, int endCol){
        List<Map<Integer,Object>> mapListResult = new ArrayList<>();

        for (int i = 0; i < mapList.size();i ++) {
            if(i >= startRow && i < endRow){
                Map<Integer,Object> mapSource = mapList.get(i);
                Map<Integer,Object> mapResult = new HashMap<>();
                for (int j = 0; j <mapSource.size(); j++) {
                    if(j >= startCol && j < endCol){
                        mapResult.put(j,mapSource.get(j));
                    }
                }
                mapListResult.add(mapResult);
            }
        }

        return mapListResult;
    }

    public static List<Map<Integer,Object>> getColMapList(List<Map<Integer,Object>> mapList){
        List<Map<Integer,Object>> mapListResult = new ArrayList<>();
        for (int j = (Integer) getMinKey(mapList.get(0)); j < mapList.get(0).keySet().size();j ++) {
                Map<Integer,Object> mapResult = new HashMap<>();
                for (int i = 0; i <mapList.size(); i++) {
                    mapResult.put(i,mapList.get(i).get(j));
                }
                mapListResult.add(mapResult);
            }

        return mapListResult;
    }

    public static List<Map<Integer,Object>> comPareMapList(List<Map<Integer,Object>> aMapList,List<Map<Integer,Object>> bMapList,Map<Integer,Integer> comPareMap){
        List<Map<Integer,Object>> mapListResult = new ArrayList<>();

        for (int i = 0; i < aMapList.size(); i++) {
            Map<Integer,Object> aMap = aMapList.get(i);
            Map<Integer,Object> bMap = bMapList.get(i);
            Map<Integer,Object> resultMap = comPareMapOpera(aMap,bMap,comPareMap);
            mapListResult.add(resultMap);
        }

        return mapListResult;
    }

    public static Map<Integer,Object> comPareMapOpera(Map<Integer,Object> aMap,Map<Integer,Object> bMap,Map<Integer,Integer> comPareMap){
        Map<Integer,Object> resultMap = new HashMap<>();

        for (int i = (Integer) getMinKey(aMap); i < aMap.size(); i++) {
            int j = comPareMap.get(i);
            if(aMap.get(i) != null && bMap.get(j) != null ){
                if(!aMap.get(i).equals(bMap.get(j))) {
                    resultMap.put(j, bMap.get(j));
                }
            }else {
                resultMap.put(j, bMap.get(j));
            }
        }

        return resultMap;
    }

    public static Object getMinKey(Map<Integer, Object> map) {
        if (map == null) {
            return null;
        }
        Set<Integer> set = map.keySet();
        Object[] obj = set.toArray();
        Arrays.sort(obj);
        return obj[0];
    }

}
