package com.luobo.pcsdepositmanage.util;


import com.luobo.pcsdepositmanage.method.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class RegisterUtils implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        register();
    }

    public void register(){
        System.out.println("RegisterUtils->register()");
        //从excel文件中获取数据并存储为List<Map<String,Object>>
        MethodTempFactory.register("logicdeposit", GetLogicDepositPara.getLogicDepositPara);
        MethodTempFactory.register("logicdepositpcs", GetLogicDepositPcsPara.getLogicDepositPcsPara);
        MethodTempFactory.register("logicdepositdestsystem", GetLogicDepositDestsystemPara.getLogicDepositDestsystemPara);
        MethodTempFactory.register("pcsdeposit", GetPcsDepositPara.getPcsDepositPara);
        MethodTempFactory.register("pcsdepositdestsystem", GetPcsDepositDestsystemPara.getPcsDepositDestsystemPara);
        MethodTempFactory.register("pcsdepositlogic", GetPcsDepositLogicPara.getPcsDepositLogicPara);
        MethodTempFactory.register("pcsdepositresult", GetPcsDepositResultPara.getPcsDepositResultPara);

        MethodTempFactory.register("pcssourcemap", GetPcsSourceMapPara.getPcsSourceMapPara);
        MethodTempFactory.register("ljjkservice", GetLjjkServicePara.getLjjkServicePara);

        MethodTempFactory.register("sysdictdata", GetSysDictDataPara.getSysDictDataPara);


    }
}
