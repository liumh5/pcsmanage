package com.luobo.pcsdepositmanage.util;

import com.luobo.pcsdepositmanage.method.MethodTempFactory;
import com.luobo.pcsdepositmanage.method.MethodTemplate;

import java.util.Map;

public class MethodTempUtils {

    private static MethodTemplate methodTemplate = null;

    public static Map<String, Object> getParaMapMethod(Map<String, Object> map) {

        Map<String, Object> paraMap = null;

        String methodName = null;
        if (map.containsKey("methodName")) {
            methodName = (String) map.get("methodName");
        }else{
            throw new UnsupportedOperationException("map方法缺少methodName键值");
        }
        methodTemplate = MethodTempFactory.getInvokeStrategy(methodName);

        if(methodTemplate != null){
            try {
                paraMap = methodTemplate.getParaMapTemplate(map);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            throw new UnsupportedOperationException("methodTemplate未发现方法："+methodName);
        }

        return paraMap;

    }
}
