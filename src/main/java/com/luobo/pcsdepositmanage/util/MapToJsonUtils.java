package com.luobo.pcsdepositmanage.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapToJsonUtils {




    /**
     * Map格式转换成json
     * @param map
     * @return
     * @throws Exception
     */
    public static JSONObject mapToJson(Map<String,Object> map) throws Exception{
        return  JSON.parseObject(mapToStringJson(map));//String转json
    }

    /**
     *  Map格式转换成json格式的String
     * @param map
     * @return
     * @throws Exception
     */
    public static String mapToStringJson(Map<String,Object> map) throws Exception{
        return  JSON.toJSONString(map);//map转String
    }
    /**
     *  Map格式转换成json格式的String
     * @param map
     * @return
     * @throws Exception
     */
    public static String mapIntToStringJson(Map<Integer,Object> map) throws Exception{
        return  JSON.toJSONString(map);//map转String
    }



    /**
     * JSONObject  直接转换成Map
     * @param json
     * @return
     * @throws Exception
     */
    public static Map<String,Object> jsonToMap(JSONObject json) throws Exception{
        return JSONObject.toJavaObject(json, Map.class);
    }

    /**
     * Json格式的字符串，转换成Map格式，通过Feature.OrderedField  禁止转换过程重新排序
     * @param stringJson
     * @return Map<String,Object>
     * @throws Exception
     */
    public static Map<String,Object> stringJsonToMap(String stringJson) throws Exception{
//      json会将string顺序破坏
//        return JSON.parseObject(stringJson,Map.class);
        //禁止重新排序
        return JSON.parseObject(stringJson,Map.class , Feature.OrderedField);
    }
}
