package com.luobo.pcsdepositmanage.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.luobo.pcsdepositmanage.easyexcel.listener.consumer.EasyExcelConsumerListener;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import javafx.beans.property.adapter.JavaBeanBooleanPropertyBuilder;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.formula.functions.T;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

@NoArgsConstructor
public class ExcelUtil extends EasyExcel {

    public static <T>ExcelReaderBuilder read(String pathName, Class<T> head, Integer pageSize, ConsumerUtil<List<T>> consumer){
        return read(pathName,head,new EasyExcelConsumerListener(pageSize,consumer));
    }
    public static <T>ExcelReaderBuilder read(File file, Class<T> head, Integer pageSize, ConsumerUtil<List<T>> consumer){
        return read(file,head,new EasyExcelConsumerListener(pageSize,consumer));
    }
    public static <T>ExcelReaderBuilder read(InputStream inputStream, Class<T> head, Integer pageSize, ConsumerUtil<List<T>> consumer){
        return read(inputStream,head,new EasyExcelConsumerListener(pageSize,consumer));
    }


}
