package com.luobo.pcsdepositmanage.controller;

import com.alibaba.excel.annotation.ExcelProperty;
import com.luobo.pcsdepositmanage.pojo.basic.*;
import com.luobo.pcsdepositmanage.service.basic.*;
import com.luobo.pcsdepositmanage.util.MapToJsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class queryLogicController {

    @Autowired
    private LogicDepositService logicDepositService;
    @Autowired
    private LogicDepositPcsService logicDepositPcsService;
    @Autowired
    private LogicDepositDestsystemService logicDepositDestsystemService;
    @Autowired
    private PcsDepositService pcsDepositService;
    @Autowired
    private PcsDepositLogicService pcsDepositLogicService;
    @Autowired
    private PcsDepositDestsystemService pcsDepositDestsystemService;
//        resultMap = logicDepositList.stream().collect(Collectors.toMap(LogicDeposit::getLogicDepositCode,LogicDeposit::getLogicDepositName,(key1,key2) -> key2));

    @RequestMapping("/querylogic/{logicdepositcode}")
    public String querylogic(@PathVariable("logicdepositcode") String logicdepositcodepara){

        Map<String,String> paraMap = new HashMap<>();
        paraMap.put("logicDepositCode",logicdepositcodepara);
        List<LogicDeposit> logicDepositList = logicDepositService.queryDataForMap(paraMap);

        Map<String,Object> resultMap =  resultLogicDeposits(logicDepositList);
        if (resultMap.keySet().size() == 0){
            resultMap.put("未获取到任何数据",logicdepositcodepara);
        }
        String resultString = null;
        try {
            resultString = MapToJsonUtils.mapToStringJson(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }


    @RequestMapping("/querypcs/{pcsdepositcode}")
    public String querypcs(@PathVariable("pcsdepositcode") String pcsdepositcodepara){

        Map<String,String> paraMap = new HashMap<>();

        paraMap.put("pcsDepositCode",pcsdepositcodepara);
        List<PcsDeposit> pcsDepositList = pcsDepositService.queryDataForMap(paraMap);


        Map<String,Object> resultMap =  resultPcsDeposits(pcsDepositList);

        if (resultMap.keySet().size() == 0){
            resultMap.put("未获取到任何数据",pcsdepositcodepara);
        }
        String resultString = null;
        try {
            resultString = MapToJsonUtils.mapToStringJson(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }


    public Map<String,Object> resultLogicDeposits(List<LogicDeposit> logicDepositList){
        Map<String,Object> resultMap = new HashMap<>();

        int Logic_deposit_id = 0;
        String logicDepositCode = null;
        String logicDepositName = null;
        String logicDepositStatus = null;

        for (LogicDeposit logicDeposit:
                logicDepositList) {

            Logic_deposit_id = logicDeposit.getLogic_deposit_id();
            logicDepositCode = logicDeposit.getLogicDepositCode();
            logicDepositName = logicDeposit.getLogicDepositName();
            logicDepositStatus = logicDeposit.getLogicDepositStatus();
            Map<String,String> paraMap = new HashMap<>();

            paraMap.put("logicDepositCode",logicDepositCode);
            List<LogicDepositPcs> logicDepositPcsList = logicDepositPcsService.queryDataForMap(paraMap);
            List<LogicDepositDestsystem> logicDepositDestsystemList = logicDepositDestsystemService.queryDataForMap(paraMap);

            List<String> systemNameList = new ArrayList<>();
            for (LogicDepositDestsystem logicDepositDestsystem :
                    logicDepositDestsystemList) {
                String logicDepositDestsystemCode =  logicDepositDestsystem.getLogicDepositCode();
                String systemName =  logicDepositDestsystem.getSystemName();

                if (logicDepositCode.equals(logicDepositDestsystemCode)){
                    systemNameList.add(systemName);
                }
            }
            List<String> pcsDepositList = new ArrayList<>();
            for (LogicDepositPcs logicDepositPcs :
                    logicDepositPcsList) {
                String logicDepositPcsCode =  logicDepositPcs.getLogicDepositCode();
                String pcsDepositCode =  logicDepositPcs.getPcsDepositCode()+"："+logicDepositPcs.getPcsDepositName();

                if (logicDepositCode.equals(logicDepositPcsCode)){
                    pcsDepositList.add(pcsDepositCode);
                }
            }

            resultMap.put("逻辑接口信息：","序号："+Logic_deposit_id
                    +"，逻辑接口编码："+logicDepositCode
                    +"，逻辑接口名字："+logicDepositName
                    +"，承接状态："+logicDepositStatus);
            resultMap.put("关联系统列表",systemNameList);
            resultMap.put("PCS服务列表",pcsDepositList);

        }
        return resultMap;
    }

    public Map<String, Object> resultPcsDeposits(List<PcsDeposit> pcsDepositList) {
        Map<String, Object> resultMap = new HashMap<>();
        int pcs_deposit_id = 0;
        String pcsDepositCode = null;
        String pcsDepositName = null;
        System.out.println(pcsDepositList.size());
        System.out.println(pcsDepositList);

        for (PcsDeposit pcsDeposit:
                pcsDepositList) {

            pcs_deposit_id = pcsDeposit.getPcs_deposit_id();
            pcsDepositCode = pcsDeposit.getPcsDepositCode();
            pcsDepositName = pcsDeposit.getPcsDepositName();
            Map<String,String> paraMap = new HashMap<>();

            paraMap.put("pcsDepositCode",pcsDepositCode);

            List<PcsDepositLogic> pcsDepositLogicList = pcsDepositLogicService.queryDataForMap(paraMap);
            List<PcsDepositDestsystem> pcsDepositDestsystemList = pcsDepositDestsystemService.queryDataForMap(paraMap);

            List<String> systemNameList = new ArrayList<>();

            for (PcsDepositDestsystem pcsDepositDestsystem :
                    pcsDepositDestsystemList) {
                String pcsDepositDestsystemCode =  pcsDepositDestsystem.getPcsDepositCode();
                String systemName =  pcsDepositDestsystem.getSystemName();

                if (pcsDepositCode.equals(pcsDepositDestsystemCode)){
                    systemNameList.add(systemName);
                }
            }
            List<String> logicDepositList = new ArrayList<>();
            for (PcsDepositLogic pcsDepositLogic :
                    pcsDepositLogicList) {
                String pcsDepositLogicCode =  pcsDepositLogic.getPcsDepositCode();
                String logicDepositCode =  pcsDepositLogic.getLogicDepositCode()+"："+pcsDepositLogic.getLogicDepositName();

                if (pcsDepositCode.equals(pcsDepositLogicCode)){
                    logicDepositList.add(logicDepositCode);
                }
            }

            resultMap.put("PCS服务信息：","序号："+pcs_deposit_id
                    +"，PCS服务编码："+pcsDepositCode
                    +"，PCS服务名字："+pcsDepositName);
            resultMap.put("关联系统列表",systemNameList);
            resultMap.put("逻辑接口列表",logicDepositList);

        }

        return resultMap;

    }

}

