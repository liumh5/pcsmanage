package com.luobo.pcsdepositmanage.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.luobo.pcsdepositmanage.easyexcel.listener.basic.*;
import com.luobo.pcsdepositmanage.method.*;
import com.luobo.pcsdepositmanage.pojo.basic.*;
import com.luobo.pcsdepositmanage.service.DbService;
import com.luobo.pcsdepositmanage.service.basic.*;
import com.luobo.pcsdepositmanage.util.EasyExcelUtils;
import com.luobo.pcsdepositmanage.util.MapToJsonUtils;
import com.luobo.pcsdepositmanage.util.MethodTempUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class easyExcelController {
    @Value("${filedir.name1}")
    private String mrdir;
    @Autowired
    private LogicDepositService logicDepositService;
    @Autowired
    private LogicDepositPcsService logicDepositPcsService;
    @Autowired
    private LogicDepositDestsystemService logicDepositDestsystemService;
    @Autowired
    private PcsDepositService pcsDepositService;
    @Autowired
    private PcsDepositDestsystemService pcsDepositDestsystemService;


    @RequestMapping("/readfile/{methodname}/{getdir}/{getfile}/{getsheet}")
    public String updateMysqlFromExcel(@PathVariable("methodname") String methodname,@PathVariable("getdir") String getdir, @PathVariable("getfile") String getfile, @PathVariable("getsheet") String getsheet) throws Exception {
//        Map<String, Object> paraMap = getParaMap(getdir, getfile, getsheet);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("methodName", methodname);

        if(!getdir.equals("default")){
            map.put("filedir",getdir);
        }
        if(!getfile.equals("default")){
            map.put("filename",getdir);
        }
        if(!getdir.equals("default")){
            map.put("sheetname",getdir);
        }

        Map<String, Object> paraMap = MethodTempUtils.getParaMapMethod(map);

        Map<String, Object> resultMap = new HashMap<String, Object>();

        resultMap.put("传入文件名", paraMap.get("filename"));
        resultMap.put("传入sheet名", paraMap.get("sheetname"));

        int addafternum = 0;
        if (paraMap.containsKey("clazz")) {
            DbService<T> servicename = (DbService) paraMap.get("servicename");
//            servicename.delData(null);
            EasyExcelUtils.readForEasyExcel(paraMap);
            addafternum = servicename.queryCount(null);
        }

        resultMap.put("导入excle文件后数量", addafternum);

        return MapToJsonUtils.mapToStringJson(resultMap);
    }

    @RequestMapping("/writefile/{methodname}/{getdir}/{getfile}/{getsheet}")
    public String writeExcelFromMysql(@PathVariable("methodname") String methodname, @PathVariable("getdir") String getdir, @PathVariable("getfile") String getfile, @PathVariable("getsheet") String getsheet) throws Exception {
//        Map<String, Object> paraMap = getParaMap(getdir, getfile, getsheet);
        Map<String, Object> resultMap = new HashMap<String, Object>();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("methodName", methodname);

        if(!getdir.equals("default")){
            map.put("filedir",getdir);
        }
        if(!getfile.equals("default")){
            map.put("filename",getdir);
        }
        if(!getdir.equals("default")){
            map.put("sheetname",getdir);
        }

        Map<String, Object> paraMap = MethodTempUtils.getParaMapMethod(map);

        DbService<T> servicename = (DbService) paraMap.get("servicename");
        int addafternum = servicename.queryCount(null);
        resultMap.put("数据库导出文件数量", addafternum);

        List<T> objectList = servicename.queryAllData();
        if (paraMap.containsKey("clazz")) {
            EasyExcelUtils.writeToEasyExcel(paraMap, objectList);
        }

        return MapToJsonUtils.mapToStringJson(resultMap);
    }



//    public Map<String, Object> getParaMap(String getdir, String getfile, String getsheet) {
//
//        Map<String, Object> paraMap = new HashMap<String, Object>();
//
//        String filedir = null;
//        String filename = null;
//        String sheetname = null;
//        if (getdir.equals("moren")) {
//            filedir = mrdir;
//        } else {
//            filedir = mrdir + getdir;
//        }
//
//        if (getfile.equals("logicdeposit")) {
//            filename = "逻辑接口信息表.xlsx";
//            paraMap.put("clazz", LogicDeposit.class);
//            paraMap.put("listener", new LogicDepositListener(logicDepositService));
//            paraMap.put("servicename", logicDepositService);
//
//        } else if (getfile.equals("pcsdeposit")) {
//            filename = "新核心pcs信息表.xlsx";
//            paraMap.put("clazz", PcsDeposit.class);
//            paraMap.put("listener", new PcsDepositListener(pcsDepositService));
//            paraMap.put("servicename", pcsDepositService);
//        } else if (getfile.equals("logicdepositpcs")) {
//            filename = "逻辑接口与pcs对照关系.xlsx";
//            paraMap.put("clazz", LogicDepositPcs.class);
//            paraMap.put("listener", new LogicDepositPcsListener(logicDepositPcsService));
//            paraMap.put("servicename", logicDepositPcsService);
//
//        } else if (getfile.equals("logicdepositdestsystem")) {
//            filename = "逻辑接口与关联系统对照关系.xlsx";
//            paraMap.put("clazz", LogicDepositDestsystem.class);
//            paraMap.put("listener", new LogicDepositDestsystemListener(logicDepositDestsystemService));
//            paraMap.put("servicename", logicDepositDestsystemService);
//
//        } else if (getfile.equals("pcsdepositdestsystem")) {
//            filename = "新核心pcs与关联系统对照关系.xlsx";
//            paraMap.put("clazz", PcsDepositDestsystem.class);
//            paraMap.put("listener", new PcsDepositDestsystemListener(pcsDepositDestsystemService));
//            paraMap.put("servicename", pcsDepositDestsystemService);
//        } else {
//            filename = getfile;
//        }
//
//        if (getsheet.equals("logicdeposit")) {
//            sheetname = "逻辑接口";
//        } else if (getsheet.equals("pcsdeposit")) {
//            sheetname = "新核心接口";
//        } else if (getsheet.equals("logicdepositpcs")) {
//            sheetname = "逻辑接口与新核心接口";
//        } else if (getsheet.equals("logicdepositdestsystem")) {
//            sheetname = "逻辑接口与关联系统";
//        } else if (getsheet.equals("pcsdepositdestsystem")) {
//            sheetname = "新核心接口与关联系统";
//        } else {
//            sheetname = getsheet;
//        }
//
//        paraMap.put("filename", filename);
//        paraMap.put("filedir", filedir);
//        paraMap.put("sheetname", sheetname);
//
//        return paraMap;
//    }


}
