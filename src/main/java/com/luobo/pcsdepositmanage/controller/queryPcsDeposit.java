package com.luobo.pcsdepositmanage.controller;

import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.service.databasetest.PcsDepositFileService;
import com.luobo.pcsdepositmanage.util.MapToJsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class queryPcsDeposit {

    @Autowired
    private PcsDepositFileService pcsDepositFileService;

    @RequestMapping("/testpcs")
    public String queryPcs(){

        Map<String,Object> resultMap =  new HashMap<>();

        int queryCount = pcsDepositFileService.queryCount(null);
        resultMap.put("code",0);
        resultMap.put("msg","");
        resultMap.put("count",queryCount);

        List<PcsDepositFile> pcsDepositFiles = pcsDepositFileService.queryAllData();

        List<Map<String,Object>> pcsDepositMap = createPcsDepositMap(pcsDepositFiles);
        resultMap.put("data",pcsDepositMap);

        String resultString = null;
        try {
            resultString = MapToJsonUtils.mapToStringJson(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    private List<Map<String, Object>> createPcsDepositMap(List<PcsDepositFile> pcsDepositFiles) {

        List<Map<String, Object>> resultList = new ArrayList<>();

        int i = 1;
        for (PcsDepositFile temppcsDepositFile:
             pcsDepositFiles) {
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("id",i++);
            resultMap.put("pcsDepositCode",temppcsDepositFile.getPcsDepositCode());
            resultMap.put("pcsDepositName",temppcsDepositFile.getPcsDepositName());
            resultMap.put("pcsDepositSyscode",temppcsDepositFile.getPcsDepositSyscode());
            resultMap.put("counterType",temppcsDepositFile.getCounterType());

            resultList.add(resultMap);
        }

        return resultList;

    }
}
