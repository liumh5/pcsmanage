package com.luobo.pcsdepositmanage.controller;

import com.luobo.pcsdepositmanage.pojo.basic.LogicDeposit;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositDestsystem;
import com.luobo.pcsdepositmanage.pojo.basic.LogicDepositPcs;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import com.luobo.pcsdepositmanage.service.databasetest.PcsInOutDescribeService;
import com.luobo.pcsdepositmanage.util.MapToJsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class queryPcsInOut {

    private static final Logger LOGGER = LoggerFactory.getLogger(queryPcsInOut.class);

    @Autowired
    private PcsInOutDescribeService pcsInOutDescribeService;


    @RequestMapping("/query2/{pcsDepositCode}")
    public String queryPcsInOut(@PathVariable("pcsDepositCode") String pcsDepositCode){
        Map<String,Object> resultMap =  new HashMap<>();

        Map<String,String> paraMap = new HashMap<>();
        paraMap.put("pcsDepositCode",pcsDepositCode);

        int queryCount = pcsInOutDescribeService.queryCount(paraMap);
        resultMap.put("code",0);
        resultMap.put("msg","");
        resultMap.put("count",queryCount);

        List<PcsInOutDescribe> pcsInOutDescribes = pcsInOutDescribeService.queryDataForMap(paraMap);

        List<Map<String,Object>> pcsInOutDescribeMap = createPcsInOutDescribeMap(pcsInOutDescribes);
        resultMap.put("data",pcsInOutDescribeMap);

        String resultString = null;
        try {
            resultString = MapToJsonUtils.mapToStringJson(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    private List<Map<String, Object>> createPcsInOutDescribeMap(List<PcsInOutDescribe> pcsInOutDescribes) {

        List<Map<String, Object>> resultList = new ArrayList<>();

        int i = 1;
        for (PcsInOutDescribe temppcsInOutDescribe:
                pcsInOutDescribes) {
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("orderNum",temppcsInOutDescribe.getOrderNum());
            resultMap.put("inOutFlag",temppcsInOutDescribe.getInOutFlag()==0?"输入":"输出");
            resultMap.put("enName",temppcsInOutDescribe.getEnName());
            resultMap.put("chName",temppcsInOutDescribe.getChName());
            resultMap.put("isRequired",temppcsInOutDescribe.getIsRequired());
            resultMap.put("dataDictCode",temppcsInOutDescribe.getDataDictCode());
            resultMap.put("dataDictTypeDescribe",temppcsInOutDescribe.getDataDictTypeDescribe());
            resultMap.put("dataType",temppcsInOutDescribe.getDataType());
            resultMap.put("remarkDescribe",temppcsInOutDescribe.getRemarkDescribe());

            resultList.add(resultMap);
        }

        return resultList;



    }

    @RequestMapping("/query/{pcsDepositCode}")
    public String importDataFromACSFile(@PathVariable("pcsDepositCode") String pcsDepositCode){

        LOGGER.info("查询服务编码：{}",pcsDepositCode);
        Map<String,String> paraMap = new HashMap<>();
        paraMap.put("pcsDepositCode",pcsDepositCode);
        List<PcsInOutDescribe> pcsInOutDescribeList = pcsInOutDescribeService.queryDataForMap(paraMap);

        Map<String,Object> resultMap =  resultPcsDeposits(pcsInOutDescribeList);
        if (resultMap.keySet().size() == 0){
            resultMap.put("未获取到任何数据",pcsInOutDescribeList);
        }
        String resultString = null;
        try {
            resultString = MapToJsonUtils.mapToStringJson(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;

    }



    public Map<String,Object> resultPcsDeposits(List<PcsInOutDescribe> pcsInOutDescribeList){
        Map<String,Object> resultMap = new HashMap<>();

        String pcsDepositCode = null;
        int orderNum = 0;
        String chName = null;
        String enName;
        String isRequired;
        String dataDictCode;
        String dataDictTypeDescribe;
        String dataType;
        String remarkDescribe;
        int inOutFlag = 0;
        pcsDepositCode = pcsInOutDescribeList.get(0).getPcsDepositCode();
        Map<String,Object> resultInOutMap = new HashMap<>();

        Map<String,Object> resultInMap = new HashMap<>();
        Map<String,Object> resultOutMap = new HashMap<>();

        for (PcsInOutDescribe pcsInOutDescribe:
                pcsInOutDescribeList) {

            orderNum = pcsInOutDescribe.getOrderNum();
            chName = pcsInOutDescribe.getChName();
            enName = pcsInOutDescribe.getEnName();
            isRequired = pcsInOutDescribe.getIsRequired();
            dataDictCode = pcsInOutDescribe.getDataDictCode();
            dataDictTypeDescribe = pcsInOutDescribe.getDataDictTypeDescribe();
            dataType = pcsInOutDescribe.getDataType();
            remarkDescribe = pcsInOutDescribe.getRemarkDescribe();
            inOutFlag = pcsInOutDescribe.getInOutFlag();
            Map<String,Object> resultAllMap = new HashMap<>();

            resultAllMap.put("英文名称",enName);
            resultAllMap.put("中文名称",chName);
            resultAllMap.put("是否必输",isRequired);
            resultAllMap.put("数据项编号",dataDictCode);
            resultAllMap.put("格式长度",dataDictTypeDescribe);
            resultAllMap.put("数据类型",dataType);
            resultAllMap.put("说明",remarkDescribe);

            if(inOutFlag == 0){
                resultInMap.put(orderNum+"："+chName, resultAllMap);
            }
            if(inOutFlag == 1){
                resultOutMap.put(orderNum+"："+chName, resultAllMap);
            }
        }
        resultInOutMap.put("输入",resultInMap);
        resultInOutMap.put("输出",resultOutMap);
        resultMap.put(pcsDepositCode,resultInOutMap);
        return resultMap;
    }


}
