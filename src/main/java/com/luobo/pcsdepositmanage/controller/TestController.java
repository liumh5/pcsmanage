package com.luobo.pcsdepositmanage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

    @RequestMapping("/login")
    public String testTheathf(){
        return "login";
    }

    @RequestMapping("/index")
    public String testhello(){
        return "index";
    }

    @RequestMapping("/welcome1")
    public String welcome1(){ return "welcome1"; }

    @RequestMapping("/pcsdepositList1")
    public String pcsdepositList1(){
        return "pcsdeposit-list1";
    }
    @RequestMapping("/pcsinoutList1")
    public String pcsinoutList1(){
        return "pcsinout-list1";
    }

    @RequestMapping("/memberDel")
    public String memberDel(){
        return "member-del";
    }
    @RequestMapping("/memberAdd")
    public String memberAdd() { return "member-add"; }
    @RequestMapping("/orderList")
    public String orderList(){
        return "order-list";
    }
    @RequestMapping("/orderList1")
    public String orderList1(){
        return "order-list1";
    }

    @RequestMapping("/cate")
    public String cate(){
        return "cate";
    }
    @RequestMapping("/testsearch")
    public String testsearch(){
        return "testsearch";
    }

    @RequestMapping("/testpcs/{pcscode}")
    public String testpcs(@PathVariable("pcscode") String pcscode,Model model){

        model.addAttribute("name",pcscode);
        return "testpcs";
    }


}
