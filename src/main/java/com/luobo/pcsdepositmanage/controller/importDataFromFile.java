package com.luobo.pcsdepositmanage.controller;

import com.luobo.pcsdepositmanage.method.AbstractMethodTemp;
import com.luobo.pcsdepositmanage.method.MethodTempFactory;
import com.luobo.pcsdepositmanage.method.MethodTemplate;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsDepositFile;
import com.luobo.pcsdepositmanage.pojo.databasetest.PcsInOutDescribe;
import com.luobo.pcsdepositmanage.pojo.databasetest.SysDictData;
import com.luobo.pcsdepositmanage.service.databasetest.PcsDepositFileService;
import com.luobo.pcsdepositmanage.service.databasetest.PcsInOutDescribeService;
import com.luobo.pcsdepositmanage.service.databasetest.SysDictDataService;
import com.luobo.pcsdepositmanage.util.EasyExcelUtils;
import com.luobo.pcsdepositmanage.util.ExcelOperationUtil;
import com.luobo.pcsdepositmanage.util.MethodTempUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
public class importDataFromFile {


    private static final Logger LOGGER = LoggerFactory.getLogger(importDataFromFile.class);

    @Autowired
    private PcsInOutDescribeService pcsInOutDescribeService;
    @Autowired
    private PcsDepositFileService pcsDepositFileService;
    @Autowired
    private SysDictDataService sysDictDataService;


    @Value("${filedir.name1}")
    private String filedir;
    @Value("${filedir.acsfiledir}")
    private String acsfiledir;
    @Value("${filedir.acslistFile}")
    private String acslistFile;
    @Value("${filedir.acslistsheet}")
    private String acslistsheet;
    @Value("${filedir.acslistheadnum}")
    private int acslistheadnum;

    @Value("${filedir.sysdictfile}")
    private String sysdictFile;
    @Value("${filedir.sysdictsheet}")
    private String sysdictSheet;
    @Value("${filedir.sysdictaheadnum}")
    private int sysdictaheadnum;


    private List<Map<Integer,Object>> sysDictmapList;
    private List<Map<Integer,Object>> acsFileList;
    private Map<String,Object> acsNameMap = new HashMap<>();

    private List<PcsDepositFile> pcsDepositFileList = new ArrayList<>();
    private List<PcsInOutDescribe> pcsInOutDescribeList = new ArrayList<>();
    private List<SysDictData> sysDictDataList = new ArrayList<>();

    Map<Integer,String> converMap = new HashMap<>();




    @RequestMapping("/importsysdict")
    public String importSysDictData(){

//        读取文件
//        生成List<SysDictData>
        LOGGER.info("开始读取数据字典文件");
        readSysDictData();


//        将List<SysDictData>写入数据库
        LOGGER.info("将数据载入数据库");
        this.sysDictDataService.batchAddData(this.sysDictDataList);
        int succNum = this.sysDictDataService.queryCount(null);
        LOGGER.info("登记成功{}条数据",succNum);


        return "Succ";
    }

    private void readSysDictData() {


        Map<String,Object> paraMap = new HashMap<>();
        try {

            String filename = this.sysdictFile;
            String sheetname = this.sysdictSheet;
            int headrownum = this.sysdictaheadnum;
            paraMap.put("filedir",this.filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);

//            this.acsFileList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.sysDictmapList = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);

            for (int i = 0; i < this.sysDictmapList.size(); i++) {
                SysDictData sysDictData = new SysDictData();

                String tempDataDictCode = (String)this.sysDictmapList.get(i).get(0);
                String tempDataDictCate = (String)this.sysDictmapList.get(i).get(1);
                String tempChName = (String)this.sysDictmapList.get(i).get(4);
                String tempRegionCode = (String)this.sysDictmapList.get(i).get(14);
                String tempDataType = (String)this.sysDictmapList.get(i).get(15);
                String tempDataDictTypeDescribe = (String)this.sysDictmapList.get(i).get(17);
                String tempEnName = (String)this.sysDictmapList.get(i).get(19);

                sysDictData.setDataDictCode(tempDataDictCode);
                sysDictData.setDataDictCate(tempDataDictCate);
                sysDictData.setChName(tempChName);
                sysDictData.setRegionCode(tempRegionCode);
                sysDictData.setDataType(tempDataType);
                sysDictData.setDataDictTypeDescribe(tempDataDictTypeDescribe);
                sysDictData.setEnName(tempEnName);

                this.sysDictDataList.add(sysDictData);
            }




        } catch (Exception e) {
            e.printStackTrace();
        }



    }
    @RequestMapping("/importsysdictee")
    public String readSysDictDataForEasyExcel(){
        Map<String, Object> map = new HashMap<>();

        map.put("methodName","sysdictdata");
        Map<String,Object> paraMap = MethodTempUtils.getParaMapMethod(map);

        try {
            EasyExcelUtils.readForEasyExcel(paraMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int succNum = this.sysDictDataService.queryCount(null);
        LOGGER.info("登记成功{}条数据",succNum);
        return "Succ";
    }


    @RequestMapping("/importnodict")
    public String importDataFromACSFileNoDict(){

//        读取文件
//        生成List<PcsDepositFile>
//        生成List<PcsInOutDescribe>
        readDataFromAcsFileNoDict();


//        将List<PcsInOutDescribe>写入数据库
        this.pcsDepositFileService.batchAddData(this.pcsDepositFileList);
        this.pcsInOutDescribeService.batchAddData(this.pcsInOutDescribeList);

        return "Succ";
    }

    private void readDataFromAcsFileNoDict() {
        this.converMap.put(1,"Y_0");
        this.converMap.put(2,"Y_0");
        this.converMap.put(3,"Y_0");
        this.converMap.put(4,"Y_0");
        this.converMap.put(5,"Y_0");
        this.converMap.put(6,"Y_0");
        this.converMap.put(7,"Y_0");
        this.converMap.put(8,"Y_0");
        this.converMap.put(9,"Y_0");

        Map<String,Object> paraMap = new HashMap<>();
        try {
//            this.filedir = "/Users/liuminghao/Documents/tmp";
//            String filename = "esb模板.xlsx";
//            String sheetname = "系统级字典项";

//            filename = "acsFileList.xlsx";
//            sheetname = "是否生成";
            String filename = this.acslistFile;
            String sheetname = this.acslistsheet;
            int headrownum = this.acslistheadnum;
            paraMap.put("filedir",this.filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);

//            this.acsFileList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.acsFileList = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
            for (int i = 0; i < this.acsFileList.size(); i++) {
                Object col1Object = this.acsFileList.get(i).get(0);
                Object col2Object = this.acsFileList.get(i).get(1);
                Object col3Object = this.acsFileList.get(i).get(2);
                Object col4Object = this.acsFileList.get(i).get(3);
                Object col5Object = this.acsFileList.get(i).get(4);
                Map<String,String> tempMap = new HashMap<>();

                if(col2Object != null){
                    tempMap.put("acsIsCreate",(String)col2Object);
                }else{
                    tempMap.put("acsIsCreate","否");
                }
                if(col3Object != null){
                    tempMap.put("acsDepositCate",(String)col3Object);
                }else{
                    tempMap.put("acsDepositCate","");
                }
                if(col4Object != null){
                    tempMap.put("acsDepositIdenty",(String)col4Object);
                }else{
                    tempMap.put("acsDepositIdenty","");
                }
                if(col5Object != null){
                    tempMap.put("acsDepositSyscode",(String)col5Object);
                }else{
                    tempMap.put("acsDepositSyscode","");
                }
                if(col1Object != null && col1Object.toString().length()>0){
                    this.acsNameMap.put((String)col1Object,tempMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String filedir = this.acsfiledir;
        File file = new File(filedir);
        fileOperation(file);
    }


    @RequestMapping("/import")
    public String importDataFromACSFile(){

//        读取文件
//        生成List<PcsDepositFile>
//        生成List<PcsInOutDescribe>
        readDataFromAcsFile();


//        将List<PcsInOutDescribe>写入数据库
        this.pcsDepositFileService.batchAddData(this.pcsDepositFileList);
        this.pcsInOutDescribeService.batchAddData(this.pcsInOutDescribeList);

        return "Succ";
    }


    public void readDataFromAcsFile(){
        this.converMap.put(1,"Y_0");
        this.converMap.put(2,"D_4");
        this.converMap.put(3,"Y_0");
        this.converMap.put(4,"D_1");
        this.converMap.put(5,"D_2");
        this.converMap.put(6,"D_3");
        this.converMap.put(7,"Y_0");
        this.converMap.put(8,"Y_0");
        this.converMap.put(9,"Y_0");

        Map<String,Object> paraMap = new HashMap<>();
        try {
//            this.filedir = "/Users/liuminghao/Documents/tmp";
//            String filename = "esb模板.xlsx";
//            String sheetname = "系统级字典项";

            String filename = this.sysdictFile;
            String sheetname = this.sysdictSheet;

//            int headrownum = 3;
            int headrownum = this.sysdictaheadnum;
            paraMap.put("filedir",this.filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);
//获取数据字典
//            this.sysDictmapList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.sysDictmapList = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
//            this.workbook = ExcelOperationUtil.getWorkbookFromExcel(filedir+"/"+filename);

//            filename = "acsFileList.xlsx";
//            sheetname = "是否生成";
            filename = this.acslistFile;
            sheetname = this.acslistsheet;
            headrownum = this.acslistheadnum;
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);

//            this.acsFileList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.acsFileList = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
            for (int i = 0; i < this.acsFileList.size(); i++) {
                Object col1Object = this.acsFileList.get(i).get(0);
                Object col2Object = this.acsFileList.get(i).get(1);
                Object col3Object = this.acsFileList.get(i).get(2);
                Object col4Object = this.acsFileList.get(i).get(3);
                Object col5Object = this.acsFileList.get(i).get(4);
                Object col6Object = this.acsFileList.get(i).get(5);

                Map<String,String> tempMap = new HashMap<>();

                if(col2Object != null){
                    tempMap.put("acsIsCreate",(String)col2Object);
                }else{
                    tempMap.put("acsIsCreate","否");
                }
                if(col3Object != null){
                    tempMap.put("acsDepositCate",(String)col3Object);
                }else{
                    tempMap.put("acsDepositCate","");
                }
                if(col4Object != null){
                    tempMap.put("acsDepositIdenty",(String)col4Object);
                }else{
                    tempMap.put("acsDepositIdenty","");
                }
                if(col5Object != null){
                    tempMap.put("acsDepositSyscode",(String)col5Object);
                }else{
                    tempMap.put("acsDepositSyscode","");
                }
                if(col6Object != null){
                    tempMap.put("acsDepositSAF",(String)col6Object);
                }else{
                    tempMap.put("acsDepositSAF","");
                }
                if(col1Object != null && col1Object.toString().length()>0){
                    this.acsNameMap.put((String)col1Object,tempMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String filedir = this.acsfiledir;
        File file = new File(filedir);
        fileOperation(file);
    }

    public boolean isCreateAcs(File file){
        boolean isCreateFlag = false;

        String tempFileName = file.getName();
        String[] tempStringArr = tempFileName.split("_");
        if(tempStringArr.length>3){
            String acsName = tempStringArr[3];
            if(this.acsNameMap.containsKey(acsName)){
                Map<String,String> tempString = (Map<String, String>) this.acsNameMap.get(acsName);
                if(tempString.get("acsIsCreate").equals("是")){
                    isCreateFlag = true;
                }
            }
        }


        return isCreateFlag;

    }
    public void fileOperation(File file) {
        try {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    fileOperation(f);
                }
            } else {
                int lastIndexOf = file.getName().lastIndexOf(".");
                //获取文件的后缀名 .jpg
                String suffix = file.getName().substring(lastIndexOf);
                LOGGER.info("当前文件扩展名为：{}",suffix);
                if(suffix.equals(".xlsx")){
//                    System.out.println(file.getName());
                    if(isCreateAcs(file)){
                        operaAcsFile(file);
                    }else {
                        LOGGER.info("文件：{}，不再生成列表acsFileList.xlsx中",file.getName());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    public void operaAcsFile(File file){
        Map<String,Object> paraMap = new HashMap<>();
//        配置读取excel参数
        String filedir = null;
        filedir = file.getParentFile().getPath();
        String filename = file.getName();
        paraMap.put("filedir", filedir);
        paraMap.put("filename", filename);

        Workbook tempworkbook = null;
        try {
            tempworkbook = ExcelOperationUtil.getWorkbookFromExcel(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Map<Integer, Object>> acsmapList = null;
        List<Integer> areaIntList = new ArrayList<>();
        List<Map<Integer, Object>> acsmapListQT = null;
        Iterator<Sheet> sheetIterator = tempworkbook.sheetIterator();
        while (sheetIterator.hasNext()) {
            Sheet sheet = sheetIterator.next();
            String sheetname = sheet.getSheetName();

            if(sheetname.trim().equals("ACS")){
                int headrownum = 0;

                paraMap.put("sheetname", sheetname);
                paraMap.put("headrownum", headrownum);

                LOGGER.info("设置读取文件目录:{}", paraMap.get("filedir"));
                LOGGER.info("设置读取文件名称:{}", paraMap.get("filename"));
                LOGGER.info("设置读取文件sheet页:{}", paraMap.get("sheetname"));
                LOGGER.info("设置读取文件起始行号:{}", paraMap.get("headrownum"));
//        System.out.println(paraMap);
//获取ACS定义文件数据
                try {
//                    acsmapList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
                    acsmapList = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
                    LOGGER.info("读取Sheet页:{},获取到数据:{}行", paraMap.get("sheetname"), acsmapList.size());

                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.info(e.getStackTrace().toString());

                }
//读取区域范围
                for (int i = 0; i < acsmapList.size(); i++) {
                    if (acsmapList.get(i).get(0) != null && acsmapList.get(i).get(0).toString().length()>0) {
//                System.out.println(acsmapList.get(i).get(0));
                        areaIntList.add(i);
                    }
                }
//        System.out.println(areaIntList);
            }else if (sheetname.trim().equals("ACS-其他属性")){
//                sheetname = "ACS-其他属性";
                int pageNum = tempworkbook.getSheetIndex(sheet);
                int headrownum = 0;
                paraMap.put("sheetname", pageNum);
                paraMap.put("headrownum", headrownum);

                LOGGER.info("设置读取文件sheet页:{}", paraMap.get("sheetname"));
//获取ACS定义文件ACS-其他数据
                try {
//                    acsmapListQT = EasyExcelUtils.noModelReadEasyExcel(paraMap);
                    acsmapListQT = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
                    LOGGER.info("读取Sheet页:{},获取到数据:{}行", paraMap.get("sheetname"), acsmapListQT.size());
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.info(e.getStackTrace().toString());
                }
            }

        }

        try {
            if(tempworkbook != null){
                tempworkbook.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        PcsDepositFile pcsDepositFile = convertAcsFileFromList(acsmapList, areaIntList, acsmapListQT);
        pcsDepositFile.setPcsFileDir(this.filedir);
        pcsDepositFile.setPcsFileName(filename);
        this.pcsDepositFileList.add(pcsDepositFile);

        List<PcsInOutDescribe> pcsInDescribeList = convertAcsInOutDescribeFromList(pcsDepositFile.getPcsDepositCode(),acsmapList, areaIntList,4,5,"in");
        List<PcsInOutDescribe> pcsOutDescribeList = convertAcsInOutDescribeFromList(pcsDepositFile.getPcsDepositCode(),acsmapList, areaIntList,5,6,"out");
//        导入文件接口
//        List<PcsInOutDescribe> pcsFileDescribeList = convertAcsInOutDescribeFromList(pcsDepositFile.getPcsDepositCode(),acsmapList, areaIntList,8,9,"file");

        this.pcsInOutDescribeList.addAll(pcsInDescribeList);
        this.pcsInOutDescribeList.addAll(pcsOutDescribeList);
//        导入文件接口
//        this.pcsInOutDescribeList.addAll(pcsFileDescribeList);

//
    }


    public PcsDepositFile convertAcsFileFromList(List<Map<Integer,Object>> acsmapList,List<Integer> integerList,List<Map<Integer,Object>> acsmapListQT){
        PcsDepositFile pcsDepositFile = new PcsDepositFile();

        Object tempString = acsmapList.get(0).get(5);
        if(tempString != null && tempString.toString().length()>0){
            pcsDepositFile.setPcsDepositName((String) tempString);
//            LOGGER.info("当前处理PCS服务名称:{}",acsFile.getAcsDepositName());
        }else{
            pcsDepositFile.setPcsDepositName("未正确获取PCS服务名称");
            LOGGER.info("获取ACS服务名称错误");
        }

        tempString = null;
        tempString = acsmapList.get(0).get(3);
        if(tempString != null && tempString.toString().length()>0){
            pcsDepositFile.setPcsDepositCode((String) tempString);
            LOGGER.info("当前处理PCS服务编码:{}:当前处理PCS服务名称:{}",pcsDepositFile.getPcsDepositCode(),pcsDepositFile.getPcsDepositName());

        }else{
            pcsDepositFile.setPcsDepositCode("未正确获取PCS服务编码");
            LOGGER.info("获取ACS服务名称错误");
        }

        Map<String,String> tempMap = null;

        if(acsNameMap.containsKey(pcsDepositFile.getPcsDepositCode())){
            tempMap = (Map<String, String>) acsNameMap.get(pcsDepositFile.getPcsDepositCode());
            pcsDepositFile.setPcsDepositCate(tempMap.get("acsDepositCate"));
            pcsDepositFile.setPcsDepositIdenty(tempMap.get("acsDepositIdenty"));
            pcsDepositFile.setPcsDepositSyscode(tempMap.get("acsDepositSyscode"));
            pcsDepositFile.setPcsDepositSAF(tempMap.get("acsDepositSAF"));
        }else {
            pcsDepositFile.setPcsDepositCate("特殊类");
            pcsDepositFile.setPcsDepositIdenty("非账务类");
            pcsDepositFile.setPcsDepositSAF("无");
        }

        tempString = null;
        tempString = acsmapList.get(1).get(1);
        if(tempString != null && tempString.toString().length()>0){
            pcsDepositFile.setPcsDepositDescribe((String)tempString);
        }else{
            pcsDepositFile.setPcsDepositDescribe("未正确获取PCS服务功能描述");
            LOGGER.info("未正确获取PCS服务功能描述");
        }
        tempString = null;
        tempString = acsmapList.get(3).get(1);
        if(tempString != null && tempString.toString().length()>0){
            pcsDepositFile.setPcsDepositText((String)tempString);
        }else{
            pcsDepositFile.setPcsDepositText("未正确获取PCS服务功能内容");
            LOGGER.info("未正确获取PCS服务功能内容");
        }

        if(pcsDepositFile.getPcsDepositSyscode() == null || pcsDepositFile.getPcsDepositSyscode().length() == 0){
            List<String> systemList = getAreaList(acsmapList,integerList,3,4,5);
            if(systemList!=null && systemList.size()>0){
                pcsDepositFile.setPcsDepositSyscode(String.join("|",systemList));
            }else{
                LOGGER.info("获取渠道列表为空");
                pcsDepositFile.setPcsDepositSyscode("渠道列表为空");
            }
        }

        tempString = null;

        if(pcsDepositFile.getPcsDepositSAF() == null || pcsDepositFile.getPcsDepositSAF().length() == 0){
            if(acsmapListQT != null && acsmapListQT.size()>2 && acsmapListQT.get(2).size()>5){
                tempString = acsmapListQT.get(2).get(5);
            }else{
                tempString = "无";
            }
            if(tempString != null && tempString.toString().length()>0){
                pcsDepositFile.setPcsDepositSAF((String)tempString);
            }else{
                pcsDepositFile.setPcsDepositSAF("未正确获取PCS服务SAF属性内容");
                LOGGER.info("未正确获取PCS服务SAF属性内容");
            }
        }

        return pcsDepositFile;
    }

    public List<PcsInOutDescribe> convertAcsInOutDescribeFromList(String pcsDepositCode,List<Map<Integer,Object>> acsmapList,List<Integer> integerList,int startNum ,int endNum,String inOutFlag){
        List<PcsInOutDescribe> pcsInOutDescribes = new ArrayList<>();


        int j = integerList.get(startNum);
        int k  = integerList.get(endNum);
        int flagNum = 0;

        if(inOutFlag.equals("in")){
            LOGGER.info("读取并设置原始定义文件的输入内容");
            flagNum = 1;
        }else if (inOutFlag.equals("out")){
            LOGGER.info("读取并设置原始定义文件的输出内容");
            flagNum = 1;
        }else if (inOutFlag.equals("file")){
            LOGGER.info("读取并设置原始定义文件的文件内容");
            flagNum = 5;
        }

        int ordernumadd = 1;
        for (int l = j+flagNum; l < k; l++) {
            PcsInOutDescribe pcsInOutDescribe = new PcsInOutDescribe();
            pcsInOutDescribe.setPcsDepositCode(pcsDepositCode);
            int breakflag = 0;
            Object tempString = acsmapList.get(l).get(1);
            if(tempString != null && tempString.toString().length()>0 && tempString.toString().length() < 5){
//                pcsInOutDescribe.setOrderNum(Integer.valueOf((String)tempString));
                LOGGER.info("根据excle的排列顺序，自行计算并自增序号");
            }else {
                LOGGER.info("未正确获取序号");
                breakflag ++ ;
//                break;
//                acsInOutDescribe.setOrderNum(null);
            }

            tempString = null;
            tempString = acsmapList.get(l).get(2);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setEnName((String)tempString);
            }else {
                LOGGER.info("未正确获取英文名称");
                breakflag ++ ;
                pcsInOutDescribe.setEnName(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(3);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setChName((String)tempString);
            }else {
                LOGGER.info("未正确获取中文名称");
                breakflag ++ ;
                pcsInOutDescribe.setChName(null);
            }

            if (breakflag >= 3){
                LOGGER.info("从序号开始，连续三个字段为空，退出");
                break;
            }else{
                pcsInOutDescribe.setOrderNum(ordernumadd);
                ordernumadd ++;
            }

            tempString = null;
            tempString = acsmapList.get(l).get(4);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setIsRequired((String)tempString);
            }else {
                LOGGER.info("未正确获取是否必输");
                pcsInOutDescribe.setIsRequired(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(5);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setDataDictCode((String)tempString);
            }else {
                LOGGER.info("未正确获取数据字典");
                if( pcsInOutDescribe.getChName() == null || pcsInOutDescribe.getChName().equals("SUBBEGIN") || pcsInOutDescribe.getChName().equals("SUBEND")  ){
                    pcsInOutDescribe.setDataDictCode(null);
                }else {
                    pcsInOutDescribe.setDataDictCode("申请中");
                }
            }
            tempString = null;
            tempString = acsmapList.get(l).get(6);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setDataDictTypeDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取数据格式");
                pcsInOutDescribe.setDataDictTypeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(7);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setTypeDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取数据类型");
                pcsInOutDescribe.setTypeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(8);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setAttributeDescribe((String)tempString);
            }else {
//                LOGGER.info("未正确获取栏位属性");
                pcsInOutDescribe.setAttributeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(9);
            if(tempString != null && tempString.toString().length()>0){
                pcsInOutDescribe.setRemarkDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取描述");
                pcsInOutDescribe.setRemarkDescribe(null);
            }

            pcsInOutDescribe = convertAreaListList(pcsInOutDescribe);

            if(inOutFlag.equals("in")){
                pcsInOutDescribe.setInOutFlag(0);
            }else if(inOutFlag.equals("out")){
                pcsInOutDescribe.setInOutFlag(1);
            }
            pcsInOutDescribe.setStatus(0);
            pcsInOutDescribes.add(pcsInOutDescribe);
        }

        return pcsInOutDescribes;

    }



    public PcsInOutDescribe convertAreaListList(PcsInOutDescribe pcsInOutDescribe){

        String chName = pcsInOutDescribe.getChName();
        if(chName != null && chName.length() > 0){
            List<String> sysDictList = getsysDict(chName);
            if(sysDictList == null){
                return pcsInOutDescribe;
            }
            for (int j = 1; j < this.converMap.size()+1; j++) {
                String[] converMapAarry = this.converMap.get(j).split("_");
                if(converMapAarry[0].equals("Y")){
                    continue;
                }else if (converMapAarry[0].equals("D")){
                    int k = Integer.valueOf(converMapAarry[1])-1;
                    if(k < sysDictList.size()){
                        if(j == 2){
                            pcsInOutDescribe.setEnName(sysDictList.get(k));
                        }else if(j == 4){
                            pcsInOutDescribe.setDataDictCode(sysDictList.get(k));
                        }else if(j == 5){
                            pcsInOutDescribe.setDataDictTypeDescribe(sysDictList.get(k));
                        }else if(j == 6){
                            pcsInOutDescribe.setDataType(sysDictList.get(k));
                        }
                    }else{
                        continue;
                    }

                }
            }
        }

        return pcsInOutDescribe;
    }


    public List<String> getsysDict(String name){

        if(this.sysDictmapList == null){
            return null;
        }
//        sysDictmapList
        List<String> sysDictList = new ArrayList<>();

        for (int i = 0; i < this.sysDictmapList.size(); i++) {
            if(this.sysDictmapList.get(i).get(4).equals(name)){

                Object tempString = null;
                tempString = this.sysDictmapList.get(i).get(0);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = this.sysDictmapList.get(i).get(17);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = this.sysDictmapList.get(i).get(15);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = this.sysDictmapList.get(i).get(19);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }

                break;
            }
        }
        return sysDictList;
    }

    private List<String> getAreaList(List<Map<Integer, Object>> acsmapList, List<Integer> integerList, int i, int i1,int i2) {

        int j = integerList.get(i);
        int k  = integerList.get(i1);

        List<String> stringList = new ArrayList<>();
        for (int l = j+1; l < k; l++) {
            stringList.add(acsmapList.get(l).get(i2).toString());
        }

        return stringList;

    }
}
