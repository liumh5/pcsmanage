package com.luobo.pcsdepositmanage.pojo.basic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PcsDepositDestsystem {
    @ExcelProperty("序号")
    private int pcs_deposit_destsystem_id;
    @ExcelProperty("关联系统名字")
    private String systemName;
    @ExcelProperty("接口序号")
    private String pcsId;
    @ExcelProperty("PCS服务编码")
    private String pcsDepositCode;
    @ExcelProperty("PCS服务名字")
    private String pcsDepositName;
    @ExcelProperty("接口模式")
    private String interfaceModel;
    @ExcelProperty("对接状态")
    private int djStatus;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;
}
