package com.luobo.pcsdepositmanage.pojo.basic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PcsDeposit {
    @ExcelProperty("序号")
    private int pcs_deposit_id;
    @ExcelProperty("PCS服务编码")
    private String pcsDepositCode;
    @ExcelProperty("PCS服务中文名")
    private String pcsDepositName;
    @ExcelProperty("分组")
    private String pcsDepositGroup;
    @ExcelProperty("处理类型")
    private String pcsDepositType;
    @ExcelProperty("是否柜面")
    private String counterType;
    @ExcelProperty("业务类别")
    private String pcsDepositCate;
    @ExcelProperty("服务性质")
    private String pcsDepositIdenty;
    @ExcelProperty("消费方业务场景描述")
    private String pcsDepositDescription;
    @ExcelProperty("关联系统列表")
    private String pcsDepositSysname;
    @ExcelProperty("逻辑集中服务代码")
    private String logicDepositCode;
    @ExcelProperty("负责人")
    private String pcsDepositAuthor;
    @ExcelProperty("服务状态")
    private String pcsDepositStatus;

    @ExcelProperty("对外服务名字")
    private String pcsDepositNameToOut;
    @ExcelProperty("切换过渡")
    private String pcsDepositQhgd;
    @ExcelProperty("功能描述")
    private String pcsConsumerBusinesScenarios;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;
    @ExcelProperty("备注4")
    private String remarks4;
    @ExcelProperty("备注5")
    private String remarks5;
    @ExcelProperty("备注6")
    private String remarks6;
}
