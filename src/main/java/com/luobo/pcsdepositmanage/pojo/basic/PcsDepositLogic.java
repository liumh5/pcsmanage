package com.luobo.pcsdepositmanage.pojo.basic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PcsDepositLogic {
    @ExcelProperty("序号")
    private int  pcs_deposit_logic_id;
    @ExcelProperty("PCS服务编码")
    private String  pcsDepositCode;
    @ExcelProperty("PCS服务名字")
    private String  pcsDepositName;
    @ExcelProperty("逻辑接口编码")
    private String  logicDepositCode;
    @ExcelProperty("逻辑接口名字")
    private String  logicDepositName;
    @ExcelProperty("分组")
    private String largeroupName;
    @ExcelProperty("合并状态")
    private int hbStatus;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;

}
