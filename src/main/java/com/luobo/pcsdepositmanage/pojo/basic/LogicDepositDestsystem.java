package com.luobo.pcsdepositmanage.pojo.basic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogicDepositDestsystem {
    @ExcelProperty("序号")
    private int logic_deposit_destsystem_id;
    @ExcelProperty("逻辑接口编码")
    private String logicDepositCode;
    @ExcelProperty("逻辑接口名字")
    private String logicDepositName;
    @ExcelProperty("关联系统名字")
    private String systemName;
    @ExcelProperty("对接状态")
    private int djStatus;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;

}
