package com.luobo.pcsdepositmanage.pojo.basic;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LogicDeposit {
    @ExcelProperty("序号")
    private int logic_deposit_id;
    @ExcelProperty("逻辑接口编码")
    private String logicDepositCode;
    @ExcelProperty("逻辑接口名字")
    private String logicDepositName;
    @ExcelProperty("承接状态")
    private String logicDepositStatus;
    @ExcelProperty("类型")
    private String logicDepositType;
    @ExcelProperty("分类")
    private String logicDepositCate;
    @ExcelProperty("业务功能描述")
    private String logicConsumerBusinesScenarios;
    @ExcelProperty("PCS服务编码")
    private String  pcsDepositCode;
    @ExcelProperty("PCS服务名字")
    private String  pcsDepositName;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;
    @ExcelProperty("备注4")
    private String remarks4;
    @ExcelProperty("备注5")
    private String remarks5;
    @ExcelProperty("备注6")
    private String remarks6;
}
