package com.luobo.pcsdepositmanage.pojo.createacsfile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AcsFile {
    private String acsFileDir;
    private String acsFileName;
    private String acsDepositName;
    private String acsDepositCode;
    private String acsDepositDescribe;
    private String acsDepositText;
    private String acsDepositCate;
    private String acsDepositIdenty;
    private String acsDepositSyscode;
    private String acsDepositSAF;
    private List<AcsInOutDescribe> acsInDescribeList;
    private List<AcsInOutDescribe> acsOutDescribeList;
    private List<AcsInOutDescribeResult> acsInDescribeResultList;
    private List<AcsInOutDescribeResult> acsOutDescribeResultList;

    private static final Logger LOGGER = LoggerFactory.getLogger(AcsFile.class);


    public void convertAcsInOutDescribeFromList(List<Map<Integer,Object>> acsmapList,List<Integer> integerList,int startNum ,int endNum,String inOutFlag){
        int j = integerList.get(startNum);
        int k  = integerList.get(endNum);

        if(inOutFlag.equals("in")){
            LOGGER.info("读取并设置原始定义文件的输入内容");
        }else{
            LOGGER.info("读取并设置原始定义文件的输出内容");
        }

        List<AcsInOutDescribe> acsInOutDescribeList = new ArrayList<>();
        for (int l = j+1; l < k; l++) {
            AcsInOutDescribe acsInOutDescribe = new AcsInOutDescribe();
            Object tempString = acsmapList.get(l).get(1);
            if(tempString != null && tempString.toString().length()>0 && tempString.toString().length() < 5){
                acsInOutDescribe.setOrderNum((String)tempString);
            }else {
                LOGGER.info("未正确获取序号");
                break;
//                acsInOutDescribe.setOrderNum(null);
            }

            tempString = null;
            tempString = acsmapList.get(l).get(2);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setEnName((String)tempString);
            }else {
                LOGGER.info("未正确获取英文名称");
                acsInOutDescribe.setEnName(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(3);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setChName((String)tempString);
            }else {
                LOGGER.info("未正确获取中文名称");
                acsInOutDescribe.setChName(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(4);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setIsRequired((String)tempString);
            }else {
                LOGGER.info("未正确获取是否必输");
                acsInOutDescribe.setIsRequired(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(5);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setDataDictCode((String)tempString);
            }else {
                LOGGER.info("未正确获取数据字典");
                if( acsInOutDescribe.getChName() == null || acsInOutDescribe.getChName().equals("SUBBEGIN") || acsInOutDescribe.getChName().equals("SUBEND")  ){
                    acsInOutDescribe.setDataDictCode(null);
                }else {
                    acsInOutDescribe.setDataDictCode("申请中");
                }
            }
            tempString = null;
            tempString = acsmapList.get(l).get(6);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setDataDictTypeDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取数据格式");
                acsInOutDescribe.setDataDictTypeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(7);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setTypeDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取数据类型");
                acsInOutDescribe.setTypeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(8);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setAttributeDescribe((String)tempString);
            }else {
//                LOGGER.info("未正确获取栏位属性");
                acsInOutDescribe.setAttributeDescribe(null);
            }
            tempString = null;
            tempString = acsmapList.get(l).get(9);
            if(tempString != null && tempString.toString().length()>0){
                acsInOutDescribe.setRemarkDescribe((String)tempString);
            }else {
                LOGGER.info("未正确获取描述");
                acsInOutDescribe.setRemarkDescribe(null);
            }

            acsInOutDescribeList.add(acsInOutDescribe);
        }
        if(inOutFlag.equals("in")){
            this.acsInDescribeList = acsInOutDescribeList;
        }else if(inOutFlag.equals("out")){
            this.acsOutDescribeList = acsInOutDescribeList;
        }
    }

    public void convertInToResult(List<Map<Integer,Object>> sysDictmapList){
        this.acsInDescribeResultList = convertToResult(this.acsInDescribeList,sysDictmapList);;
    }

    public void convertOutToResult(List<Map<Integer,Object>> sysDictmapList){
        this.acsOutDescribeResultList = convertToResult(this.acsOutDescribeList,sysDictmapList);
    }

    public List<AcsInOutDescribeResult> convertToResult(List<AcsInOutDescribe> acsInOutDescribeList,List<Map<Integer,Object>> sysDictmapList){
        List<AcsInOutDescribeResult> acsInOutDescribeResults = new ArrayList<>();

        for (AcsInOutDescribe acsInOutDescribe:
                acsInOutDescribeList) {
            AcsInOutDescribeResult acsInOutDescribeResult = new AcsInOutDescribeResult();

            acsInOutDescribeResult.setChName(acsInOutDescribe.getChName());

            acsInOutDescribeResult.setOrderNum(acsInOutDescribe.getOrderNum());

            acsInOutDescribeResult.setIsRequired(acsInOutDescribe.getIsRequired());

            acsInOutDescribeResult.setAttributeDescribe(acsInOutDescribe.getAttributeDescribe());
            acsInOutDescribeResult.setRemarkDescribe(acsInOutDescribe.getRemarkDescribe());

            List<String> sysDictList = getsysDict(acsInOutDescribe.getChName(),sysDictmapList);

            if(sysDictList!=null && sysDictList.size()>0){
//                System.out.println(sysDictList.toString());

                acsInOutDescribeResult.setEnName(sysDictList.get(3));
                acsInOutDescribeResult.setDataDictCode(sysDictList.get(0));
                acsInOutDescribeResult.setDataDictTypeDescribe(sysDictList.get(1));
                acsInOutDescribeResult.setDataType(sysDictList.get(2));
            }else {
                acsInOutDescribeResult.setDataDictCode(acsInOutDescribe.getDataDictCode());
            }


            acsInOutDescribeResults.add(acsInOutDescribeResult);
        }

        return acsInOutDescribeResults;
    }

    public List<String> getsysDict(String name,List<Map<Integer,Object>> sysDictmapList){

//        sysDictmapList
        List<String> sysDictList = new ArrayList<>();
        for (int i = 0; i < sysDictmapList.size(); i++) {
            if(sysDictmapList.get(i).get(4).equals(name)){

                Object tempString = null;
                tempString = sysDictmapList.get(i).get(0);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = sysDictmapList.get(i).get(17);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = sysDictmapList.get(i).get(15);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }
                tempString = sysDictmapList.get(i).get(19);
                if(tempString!=null && tempString.toString().length() > 0){
                    sysDictList.add((String)tempString);
                }else {
                    sysDictList.add(null);
                }

                break;
            }
        }
        return sysDictList;
    }

}
