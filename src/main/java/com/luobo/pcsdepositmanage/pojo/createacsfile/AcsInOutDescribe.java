package com.luobo.pcsdepositmanage.pojo.createacsfile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AcsInOutDescribe {
    private String orderNum;
    private String enName;
    private String chName;
    private String isRequired;
    private String dataDictCode;
    private String dataDictTypeDescribe;
    private String typeDescribe;
    private String attributeDescribe;
    private String remarkDescribe;
}
