package com.luobo.pcsdepositmanage.pojo.databasetest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PcsInOutDescribe {
    private String pcsDepositCode;
    private int orderNum;
    private String enName;
    private String chName;
    private String isRequired;
    private String dataDictCode;
    private String dataDictTypeDescribe;
    private String dataType;
    private String typeDescribe;
    private String attributeDescribe;
    private String remarkDescribe;
    private int inOutFlag;
    private int status;
    private String modifyDate;
    private String modifyVersion;
    private String modifyRemark;
}
