package com.luobo.pcsdepositmanage.pojo.databasetest;

import com.luobo.pcsdepositmanage.pojo.createacsfile.AcsFile;
import com.luobo.pcsdepositmanage.pojo.createacsfile.AcsInOutDescribe;
import com.luobo.pcsdepositmanage.pojo.createacsfile.AcsInOutDescribeResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PcsDepositFile {
    private String pcsFileDir;
    private String pcsFileName;
    private String pcsDepositName;
    private String pcsDepositCode;
    private String pcsDepositDescribe;
    private String pcsDepositText;
    private String pcsDepositCate;
    private String pcsDepositIdenty;
    private String pcsDepositSyscode;
    private String logicDepositCode;
    private String pcsDepositSAF;
    private String counterType;
    private int status;
    private String modifyDate;
    private String modifyVersion1;
    private String modifyVersion2;
    private String modifyVersion3;
    private String modifyRemark1;
    private String modifyRemark2;
    private String modifyRemark3;

}
