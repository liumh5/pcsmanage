package com.luobo.pcsdepositmanage.pojo.databasetest;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysDictData {
//    @ExcelProperty("数据项编号")
    @ExcelProperty(index = 0)
    private String dataDictCode;
//    @ExcelProperty("数据项分类")
    @ExcelProperty(index = 1)
    private String dataDictCate;
//    @ExcelProperty("数据格式（ans）")
    @ExcelProperty(index = 17)
    private String dataDictTypeDescribe;
//    @ExcelProperty("数据类型")
    @ExcelProperty(index = 15)
    private String dataType;
//    @ExcelProperty("中文名称")
    @ExcelProperty(index = 4)
    private String chName;
//    @ExcelProperty("JAVA规范命名")
    @ExcelProperty(index = 19)
    private String enName;
//    @ExcelProperty("域编号")
    @ExcelProperty(index = 14)
    private String regionCode;

}
