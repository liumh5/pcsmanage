package com.luobo.pcsdepositmanage.pojo.resultfile;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PcsDepositResult {
    @ExcelProperty("序号")
    private int pcs_deposit_result_id;
    @ExcelProperty("分组")
    private String pcsDepositGroup;
    @ExcelProperty("服务编码")
    private String pcsDepositCode;
    @ExcelProperty("服务中文名称")
    private String pcsDepositName;
    @ExcelProperty("服务提供方")
    private String pcsProviderSyscode;
    @ExcelProperty("切换过渡")
    private String pcsProviderQhgd;
    @ExcelProperty("业务类别")
    private String pcsDepositCate;
    @ExcelProperty("服务性质")
    private String pcsDepositType;
    @ExcelProperty("服务名字")
    private String pcsDepositNameToOut;
    @ExcelProperty("提供方服务标识")
    private String pcsDepositIdenty;
    @ExcelProperty("对应原系统接口")
    private String logicDepositCode;
    @ExcelProperty("对应原系统接口名称")
    private String logicDepositName;
    @ExcelProperty("服务消费方系统")
    private String pcsConsumerSyscode;
    @ExcelProperty("消费方场景描述")
    private String pcsConsumerBusinesScenarios;
    @ExcelProperty("备注1")
    private String remarks1;
    @ExcelProperty("备注2")
    private String remarks2;
    @ExcelProperty("备注3")
    private String remarks3;
    @ExcelProperty("备注4")
    private String remarks4;
    @ExcelProperty("备注5")
    private String remarks5;
    @ExcelProperty("备注6")
    private String remarks6;
}
