package com.luobo.pcsdepositmanage.pojo.sourcefile;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LjjkService {
    @ExcelIgnore
    private int lsa_id;
    @ExcelProperty("新核心PCS服务编码")
    private String pcsServiceCode;
    @ExcelProperty("新核心PCS服务名称")
    private String pcsServiceName;
    @ExcelProperty("接口打样组接口成果")
    private String daYangName;
    @ExcelProperty("接口合并")
    private String jiekouhebing;
    @ExcelProperty("打样接口分析")
    private String daYangOk;
    @ExcelProperty("现状逻辑集中交易代码")
    private String ljjkServiceCode;
    @ExcelProperty("现状逻辑集中交易名称")
    private String ljjkServiceName;
    @ExcelProperty("个人网银系统")
    private int grwyxt;
    @ExcelProperty("手机银行系统")
    private int sjyhxt;
    @ExcelProperty("电话银行系统")
    private int dhyhxt;
    @ExcelProperty("银联前置系统")
    private int ylqzxt;
    @ExcelProperty("固话支付系统")
    private int ghzfxt;
    @ExcelProperty("国际业务系统")
    private int gjywxt;
    @ExcelProperty("POS系统")
    private int posxt;
    @ExcelProperty("统一收单系统")
    private int tysdxt;
    @ExcelProperty("同城支付系统")
    private int tczfxt;
    @ExcelProperty("储蓄短信系统")
    private int cxdxxt;
    @ExcelProperty("第三方存管系统")
    private int dsfcgxt;
    @ExcelProperty("自助银行系统")
    private int zzyhxt;
    @ExcelProperty("IC卡前置系统")
    private int icqzxt;
    @ExcelProperty("代理保险系统")
    private int dlbxxt;
    @ExcelProperty("第三方支付前置系统")
    private int dsfzfxt;
    @ExcelProperty("中间业务平台")
    private int zjywptxt;
    @ExcelProperty("人行前置系统")
    private int rhqzxt;
    @ExcelProperty("现金凭证系统")
    private int xjpzxt;
    @ExcelProperty("邮政汇兑系统")
    private int yzhdxt;
    @ExcelProperty("公司业务系统")
    private int gsywxt;
    @ExcelProperty("支付网关")
    private int zfwgxt;
    @ExcelProperty("信贷业务平台")
    private int xdywxt;
    @ExcelProperty("超级网银系统")
    private int cjwyxt;
    @ExcelProperty("协助查控综合管理系统")
    private int xzckxt;
    @ExcelProperty("移动展业系统")
    private int ydzyxt;
    @ExcelProperty("新一代中间业务平台")
    private int xydzjywxt;
    @ExcelProperty("资金清算系统")
    private int zjqsxt;
    @ExcelProperty("TSMP系统")
    private int tsmpxt;
    @ExcelProperty("邮政金融社保卡系统")
    private int yzjrsbkxt;
    @ExcelProperty("理财类业务系统")
    private int dljjxt;
    @ExcelProperty("客户信息平台")
    private int khxixt;
    @ExcelProperty("国际支付前置")
    private int gjzfxt;
    @ExcelProperty("对账与差错处理平台")
    private int cceqxt;
    @ExcelProperty("HCE系统")
    private int hcext;
    @ExcelProperty("资产保全系统")
    private int zcbqxt;
    @ExcelProperty("互金平台")
    private int hjptxt;
    @ExcelProperty("渠道二期")
    private int qdeqxt;
    @ExcelProperty("网点授权集中系统")
    private int wdsqjzxt;
    @ExcelProperty("微信银行")
    private int wxyhxt;
    @ExcelProperty("贵金属系统")
    private int gjsxt;
    @ExcelProperty("组合交易平台")
    private int zhjyxt;
}