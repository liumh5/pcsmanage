package com.luobo.pcsdepositmanage.pojo.sourcefile;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PcsSourceMap {
    @ExcelProperty("序号")
    private int serial;
    @ExcelProperty("服务编码(7为组件编码+4为服务代码)")
    private String pcsDepositCodeOld;
    @ExcelProperty("新服务编码")
    private String pcsDepositCode;
    @ExcelProperty("大组")
    private String largeroupName;
    @ExcelProperty("小组")
    private String smallGroupName;
    @ExcelProperty("处理类型")
    private String pcsDepositType;
    @ExcelProperty("发起渠道分类")
    private String pcsDepositSysname;
    @ExcelProperty("业务功能（目前是五级任务中提取的，需要根据实际情况填写组合功能）")
    private String pcsDepositBusinesScenarios;
    @ExcelProperty("新名称非柜面渠道_/柜面渠道_")
    private String pcsDepositNameNew;
    @ExcelProperty("新核心PCS服务名称")
    private String pcsDepositName;
    @ExcelProperty("接口功能描述")
    private String pcsDepositDescription;
    @ExcelProperty("逻辑集中服务代码")
    private String logicDepositCode;
    @ExcelProperty("逻辑集中服务名称")
    private String logicDepositName;
    @ExcelProperty("负责人")
    private String pcsDepositAuthor;
    @ExcelProperty("计划开始时间")
    private String planStartDate;
    @ExcelProperty("计划结束时间")
    private String planEndDate;
    @ExcelProperty("报告人")
    private String pcsDepositReporter;
    @ExcelProperty("完成状态（是否完成ACS定义）")
    private String pcsDepositStatus;
    @ExcelProperty("是否柜面")
    private String counterType;
}
