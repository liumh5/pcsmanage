package com.luobo.pcsdepositmanage;

import com.luobo.pcsdepositmanage.util.ExcelOperationUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.StringUtil;
import org.apache.tomcat.util.buf.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class pcsdepositcreatesyscode {

    private String filedir ;
    private String resultFileName =  "新核心与关联系统对照关系_结果.xlsx";

    private List<Map<Integer,Object>> pcsDepositLogicCode = new ArrayList<Map<Integer,Object>>();
    private List<Map<Integer,Object>> logicCodeSysCode = new ArrayList<Map<Integer,Object>>();

    private Workbook workbook;
    @Test
    public void createSysCode(){
        Map<String,Object> paraMap = new HashMap<>();
        try {
            this.filedir = "/Users/liuminghao/Documents/tmp";
            String filename = "数据文件-联机接口功能清单-20201204-应用集成组-个人存款.xlsx";
            String sheetname = "附件1-PCS新核心(非柜面)与逻辑接口";

            int headrownum = 1;
            paraMap.put("filedir",filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);
//获取数据字典
//            this.sysDictmapList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.pcsDepositLogicCode = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);
            sheetname = "附件3-逻辑接口现状与关联系统关系列表";
            paraMap.put("sheetname",sheetname);
            this.logicCodeSysCode =  ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);


            List<Map<Integer,Object>> pcsDepositSysCode = convertIt();

            filename = "新核心与关联系统对照关系.xlsx";
            sheetname = "新核心与关联系统对照关系";
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);

//            this.workbook = ExcelOperationUtil.getWorkbookFromExcel(filedir+File.separator+filename);

            writeExcel(pcsDepositSysCode,paraMap);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private List<Map<Integer, Object>> convertIt() {
        List<Map<Integer,Object>> resultMap = new ArrayList<>();

        Map<String, Set<String>> pcsDepositResult = new HashMap<>();
        Map<String, Set<String>> pcsDepositSysResult = new HashMap<>();

        for (int i = 0; i <this.pcsDepositLogicCode.size(); i++) {
            Map<Integer,Object> pcsDepositMap = this.pcsDepositLogicCode.get(i);
            String pcsDepositCode = (String) pcsDepositMap.get(3);

            Set<String> logicCodeSet = null;
            if(pcsDepositResult.containsKey(pcsDepositCode)){
                logicCodeSet = pcsDepositResult.get(pcsDepositCode);
            }else{
                logicCodeSet = new HashSet<>();
            }
            String logicCode = (String) pcsDepositMap.get(1);
            logicCodeSet.add(logicCode);
            pcsDepositResult.put(pcsDepositCode,logicCodeSet);

            Set<String> sysCodeSet = null;
            if(pcsDepositSysResult.containsKey(pcsDepositCode)){
                sysCodeSet  = pcsDepositSysResult.get(pcsDepositCode);
            }else {
                sysCodeSet = new HashSet<>();
            }

            for (int j = 0; j < this.logicCodeSysCode.size(); j++) {
                Map<Integer,Object> logicCodeMap = this.logicCodeSysCode.get(j);
                String logicCodeSys = (String) logicCodeMap.get(1);
                if(logicCodeSys.equals(logicCode) && logicCodeMap.get(6).toString().equals("是")){
                    String sysCode = (String)logicCodeMap.get(3);
                    sysCodeSet.add(sysCode);
                }
            }
            pcsDepositSysResult.put(pcsDepositCode,sysCodeSet);
        }

//       pcsDepositCode 一对多，系统代码以|分隔
//        for (String pcsDepositCode:
//                pcsDepositSysResult.keySet()) {
//            String sysCodeString = StringUtils.join(pcsDepositSysResult.get(pcsDepositCode),'|');
//            Map<Integer,Object> tempMap = new HashMap<>();
//            tempMap.put(0,pcsDepositCode);
//            tempMap.put(1,sysCodeString);
//            resultMap.add(tempMap);
//        }

        //       pcsDepositCode 一对一，系统代码多行
        for(String pcsDepositCode: pcsDepositSysResult.keySet()){
            Set<String> sysCodeSet = pcsDepositSysResult.get(pcsDepositCode);
            for(String sysCodeString : sysCodeSet){
                Map<Integer,Object> tempMap = new HashMap<>();
                tempMap.put(0,pcsDepositCode);
                tempMap.put(1,sysCodeString);
                resultMap.add(tempMap);
            }
        }

        return resultMap;
    }



    @Test
    public void createSysCode2(){
        Map<String,Object> paraMap = new HashMap<>();
        try {
            this.filedir = "/Users/liuminghao/Documents/tmp";
            String filename = "数据文件-联机接口功能清单-20201204-应用集成组-个人存款.xlsx";
            String sheetname = "附件4-新核心联机接口与关联系统对照关系";
//            String filename = "aaa.xlsx";
//            String sheetname = "Sheet2";
            int headrownum = 1;
            paraMap.put("filedir",filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);
//获取数据字典
//            this.sysDictmapList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.pcsDepositLogicCode = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);


            List<Map<Integer,Object>> pcsDepositSysCode = convertIt2();

            filename = "新核心与关联系统对照关系3.xlsx";
            sheetname = "新核心与关联系统对照关系2";
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);

//            this.workbook = ExcelOperationUtil.getWorkbookFromExcel(filedir+File.separator+filename);

            writeExcel(pcsDepositSysCode,paraMap);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private List<Map<Integer, Object>> convertIt2() {
        List<Map<Integer,Object>> resultMap = new ArrayList<>();

        Map<String, Set<String>> pcsDepositResult = new HashMap<>();
        Map<String, Set<String>> pcsDepositSysResult = new HashMap<>();

        for (int i = 0; i <this.pcsDepositLogicCode.size(); i++) {
            Map<Integer,Object> pcsDepositMap = this.pcsDepositLogicCode.get(i);
            if(!pcsDepositMap.get(4).equals("是")){
                continue;
            }
            String pcsDepositCode = (String) pcsDepositMap.get(1);
            String sysCode = (String) pcsDepositMap.get(3);

            Set<String> sysCodeSet = null;
            if(pcsDepositSysResult.containsKey(pcsDepositCode)){
                sysCodeSet  = pcsDepositSysResult.get(pcsDepositCode);
            }else {
                sysCodeSet = new HashSet<>();
            }

            sysCodeSet.add(sysCode);
            pcsDepositSysResult.put(pcsDepositCode,sysCodeSet);
        }
        Map<Integer,Object> headMap = new HashMap<>();
        headMap.put(0,"服务编码");
        headMap.put(1,"系统代码");
        resultMap.add(headMap);


//       pcsDepositCode 一对多，系统代码以|分隔
        for (String pcsDepositCode:
                pcsDepositSysResult.keySet()) {
            String sysCodeString = StringUtils.join(pcsDepositSysResult.get(pcsDepositCode),'|');
            Map<Integer,Object> tempMap = new HashMap<>();
            tempMap.put(0,pcsDepositCode);
            tempMap.put(1,sysCodeString);
            resultMap.add(tempMap);
        }


        return resultMap;
    }



    @Test
    public void createSysCode3(){
        Map<String,Object> paraMap = new HashMap<>();
        try {
            this.filedir = "/Users/liuminghao/Documents/tmp";
//            String filename = "数据文件-联机接口功能清单-20201204-应用集成组-个人存款.xlsx";
//            String sheetname = "附件4-新核心联机接口与关联系统对照关系";
            String filename = "aaa.xlsx";
            String sheetname = "Sheet2";
            int headrownum = 1;
            paraMap.put("filedir",filedir);
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);
            paraMap.put("headrownum",headrownum);
//获取数据字典
//            this.sysDictmapList = EasyExcelUtils.noModelReadEasyExcel(paraMap);
            this.pcsDepositLogicCode = ExcelOperationUtil.readExcelFromFileForMapToListMap(paraMap);


            List<Map<Integer,Object>> pcsDepositSysCode = convertIt3();

            filename = "新核心与关联系统对照关系3.xlsx";
            sheetname = "新核心与关联系统对照关系2";
            paraMap.put("filename",filename);
            paraMap.put("sheetname",sheetname);

//            this.workbook = ExcelOperationUtil.getWorkbookFromExcel(filedir+File.separator+filename);

            writeExcel(pcsDepositSysCode,paraMap);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    private List<Map<Integer, Object>> convertIt3() {
        List<Map<Integer,Object>> resultMap = new ArrayList<>();

        Map<String, Set<String>> pcsDepositResult = new HashMap<>();
        Map<String, List<String>> pcsDepositSysResult = new HashMap<>();

        for (int i = 0; i <this.pcsDepositLogicCode.size(); i++) {
            Map<Integer,Object> pcsDepositMap = this.pcsDepositLogicCode.get(i);
            if(!pcsDepositMap.get(2).equals("是")){
                continue;
            }
            String pcsDepositCode = (String) pcsDepositMap.get(0);
            String sysCode = (String) pcsDepositMap.get(1);

            if(sysCode.contains("|")){
                String[] sysCodeString = sysCode.split("\\|");
                List<String> sysCodeArray = Arrays.asList(sysCodeString);
                pcsDepositSysResult.put(pcsDepositCode,sysCodeArray);
            }else {
                List<String> sysCodeArray = new ArrayList<>();
                sysCodeArray.add(sysCode);
                pcsDepositSysResult.put(pcsDepositCode,sysCodeArray);
            }

        }
        Map<Integer,Object> headMap = new HashMap<>();
        headMap.put(0,"服务编码");
        headMap.put(1,"系统代码");
        resultMap.add(headMap);

//       pcsDepositCode 一对多，系统代码以|分隔
        for (String pcsDepositCode:
                pcsDepositSysResult.keySet()) {
            List<String> tempSysCodes = pcsDepositSysResult.get(pcsDepositCode);
            for (String tempSysCode :
                    tempSysCodes) {
                Map<Integer,Object> tempMap = new HashMap<>();
                tempMap.put(0,pcsDepositCode);
                tempMap.put(1,tempSysCode);
                resultMap.add(tempMap);
            }

        }

        return resultMap;
    }





    public void writeExcel(List<Map<Integer,Object>> mapList, Map<String,Object> map){
        try {
            ExcelOperationUtil.writeListMapToWorkbookByMap(mapList,map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test(){
        int i = 12;
        //        i = i + (i- (i*i))
        System.out.println(i+=i-=i*=i);
    }
}
